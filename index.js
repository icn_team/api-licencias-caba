require("colors");
require("dotenv").config();

var app = require("./src/app");
var server = require("http").Server(app);
var port = process.env.PORT || 5000;

//si el .env tiene la variable PORT expone el puerto indicado
if(process.env.PORT) {
  server.listen(port, () => {
    console.log("API-LICENCIAS-ONLINE corriendo en puerto:" + port);
  });

  server.setTimeout(10000);
  
} 