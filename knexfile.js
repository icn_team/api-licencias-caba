// Update with your config settings.
require("dotenv").config();
const  mdb  =  require ('knex-mariadb'); 

const { knexSnakeCaseMappers } = require("objection");

module.exports = {
  development: {
    client: "mysql",
    debug: false,
    connection: {
      host : process.env.DB_HOST,
      port : process.env.DB_PORT,
      user : process.env.DB_USER,
      password : process.env.DB_PASSWORD,
      database : process.env.DB_DATABASE,
      // charset: 'utf8'
      // multipleStatements: true
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "migrations",
      directory: "./src/infra/database/migrations",
    },
    seeds: {
      directory: "./src/infra/database/seeds",
    },
    //...knexSnakeCaseMappers(),
  },

  production: {
    client: "mysql",
    debug: false,
    connection: {
      host : process.env.DB_HOST,
      port : process.env.DB_PORT,
      user : process.env.DB_USER,
      password : process.env.DB_PASSWORD,
      database : process.env.DB_DATABASE,
    },
    pool: {
      min: 2,
      max: 20,
    },
    migrations: {
      tableName: "migrations",
      directory: "./src/infra/database/migrations",
    },
    seeds: {
      directory: "./src/infra/database/seeds",
    },
    //...knexSnakeCaseMappers(),
  },
};
