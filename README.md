# API-licencias-CABA

Back-end del sistema de licencias online de CABA 

##  Requerimientos
>- node.js (14.17.5)
>- npm
>- [pm2](https://pm2.keymetrics.io/). 
>- npm install -g knex  (instala knex globalmente)


## Dependencias
>- [Objection.js](https://vincit.github.io/objection.js)
>- [mocha test framework](https://mochajs.org)

## Configuración
>- cd/{proyect_path}
>- npm install
>- Crear un archivo de entorno .env con las mismas variables del archivo .env.example
>- Crear la DB y configurar las variables de conexión en el archivo. env
>- Correr las migraciones: <b>knex migrate:latest</b>
>- Correr los seeders: <b>knex seed:run</b>

## Iniciar aplicación
- pm2 start index.js --name=api-licencias-caba --watch

## Documentación de la API
- baseUrl/api-docs/