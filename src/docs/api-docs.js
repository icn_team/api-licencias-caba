require("dotenv").config();
const swaggerJsDoc = require("swagger-jsdoc");
//const swaggerUi = require("swagger-ui-express");

// Extended: https://swagger.io/specification/#infoObject
const swaggerOptions = {
    swaggerDefinition: {
      info: {
        version: "1.0.0",
        title: "API-licencias-caba",
        description: "API-licencias-caba",
        // contact: {
        //     name: "Alejandro Espinola",
        //     email: "alexaespinola@gmail.com"
        // },
      },
      servers: [
        process.env.URL+':'+process.env.PORT,
      ],
      tags:[
        'test'
      ],
      securityDefinitions:{
        api_key:{
          type: "apiKey",
          name: "Authorization",
          in: "header",
        }
      }
    },
    apis: [
      "src/app.js",
      "src/interfaces/http/modules/auth/*.js",
      //"src/interfaces/http/modules/importaExcel/*.js",
      "src/interfaces/http/modules/tiposTramites/*.js",
      "src/interfaces/http/modules/temas/*.js",
      "src/interfaces/http/modules/modulos/*.js",
      "src/interfaces/http/modules/tiposLicencias/*.js",
      "src/interfaces/http/modules/tramites/*.js",
      "src/interfaces/http/modules/contenidos/*.js",
      //"src/interfaces/http/modules/preguntas/*.js",
      "src/interfaces/http/modules/autoTests/*.js",
      "src/interfaces/http/modules/autoTestsPreguntas/*.js",
      "src/interfaces/http/modules/scorm/*.js",
      "src/interfaces/http/modules/imagenes/*.js",
    ],
    
};

module.exports = swaggerJsDoc(swaggerOptions);