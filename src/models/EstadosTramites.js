const { Model } = require("./model");


class EstadosTramites extends Model {
  static get tableName() {
    return "estados_tramites";
  }

  /** relaciones */
  static get relationMappings() {

    const Tramites = require("./Tramites");

    return {
      tramites: {
        relation: Model.HasManyRelation,
        modelClass: Tramites,
        join: {
          from: "estados_tramites.id",
          to: "tramites.id_estado_tramite",
        },
      },
    }

  }

  /** esquema de validaciones */
  static get jsonSchema() {
    return {
      type: "object",
      additionalProperties: false,
      required: [
        "nombre",
      ],
      properties: {
        id: {
          type: "integer",
        },
        nombre: {
          type: "string",
        },
      },
    };
  }

}

module.exports = EstadosTramites;
