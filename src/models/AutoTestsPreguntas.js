const { Model } = require("./model");


class AutoTestsPreguntas extends Model {
  static get tableName() {
    return "auto_tests_preguntas";
  }

  static get idColumn() {
    return 'id';
  }

  /** relaciones */
  static get relationMappings() {

    // const AutoTest = require("./AutoTests");

    // return {


    //   auto_test: {
    //     relation: Model.HasManyRelation,
    //     modelClass: AutoTest,
    //     join: {
    //       from: "auto_tests_preguntas.id_auto_test",
    //       to: "auto_tests.id",
    //     },
    //   }, 
           
    // };

  }

  /** esquema de validaciones */
  static get jsonSchema() {
    return {
      type: "object",
      additionalProperties: false,
      required: [
        // "id_auto_test",
        // "id_pregunta",
        // "rta",
        // "data"
      ],
      properties: {
        id_auto_test: {
          type: "integer",
          minimum: 1,
        },
        id_pregunta: {
          type: "integer",
          minimum: 1,
        },
        rta: {
          type: "integer",
        },
        data_copia: {
          type: "object",
        } 
      }, //properties
    }; //return
  }

}

module.exports = AutoTestsPreguntas;
