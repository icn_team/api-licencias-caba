const { Model } = require("./model");


class Preguntas extends Model {
  static get tableName() {
    return "preguntas";
  }

  /** relaciones */
  static get relationMappings() {

    const Temas = require("./Temas");
    //const AutoTests = require("./AutoTests");
    const TiposLicencias = require("./TiposLicencias");

    return {
      tema: {
        relation: Model.BelongsToOneRelation,
        modelClass: Temas,
        join: {
          from: "preguntas.id_tema",
          to: "temas.id",
        },
      },
      // auto_tests: {
      //   relation: Model.ManyToManyRelation,
      //   modelClass: AutoTests,
      //   join: {
      //     from: 'preguntas.id',
      //     through: {
      //       from: 'auto_tests_preguntas.id_pregunta',
      //       to: 'auto_tests_preguntas.id_auto_test'
      //     },
      //     to: 'auto_tests.id'
      //   }
      // },
      tipos_licencias: {
        relation: Model.ManyToManyRelation,
        modelClass: TiposLicencias,
        join: {
          from: 'preguntas.id',
          through: {
            from: 'preguntas_tipos_licencias.id_pregunta',
            to: 'preguntas_tipos_licencias.id_tipo_licencia'
          },
          to: 'tipos_licencias.id'
        }
      }      
    }


  }

  /** esquema de validaciones */
  static get jsonSchema() {
    return {
      type: "object",
      additionalProperties: false,
      required: [
        "id_tema",
        "data"
      ],
      properties: {
        id: {
          type: "integer",
        },
        id_tema: {
          type: "integer",
          minimum: 1,
        }, 
        data: {
          type: "object",
        } 

      },
    };
  }

}

module.exports = Preguntas;
