const { Model } = require("./model");


class TramitesTiposLicencias extends Model {
  static get tableName() {
    return "tramites_tipos_licencias";
  }

  static get idColumn() {
    return 'id_tramite';
  }

  /** esquema de validaciones */
  static get jsonSchema() {
    return {
      type: "object",
      additionalProperties: false,
      required: [
        "id_tramite",
        "id_tipo_licencia"
      ],
      properties: {
        id_tramite: {
          type: "integer",
          minimum: 1,
        },
        id_tipo_licencia: {
          type: "integer",
          minimum: 1,
        },
      }, //properties
    }; //return
  }

}

module.exports = TramitesTiposLicencias;
