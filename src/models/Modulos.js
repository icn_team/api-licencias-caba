const { Model } = require("./model");


class Modulos extends Model {
  static get tableName() {
    return "modulos";
  }

  static get relationMappings () {

    const Contenidos = require("./Contenidos");
    const TiposLicencias = require("./TiposLicencias");

    return {
      contenidos: {
        relation: Model.ManyToManyRelation,
        modelClass: Contenidos,
        join: {
          from: 'modulos.id',
          through: {
            from: 'modulos_contenidos.id_modulo',
            to: 'modulos_contenidos.id_contenido',
            extra: ['orden']
          },
          to: 'contenidos.id'
        }
      },
      tipos_licencias: {
        relation: Model.ManyToManyRelation,
        modelClass: TiposLicencias,
        join: {
          from: 'modulos.id',
          through: {
            from: 'tipos_licencias_modulos.id_modulo',
            to: 'tipos_licencias_modulos.id_tipo_licencia'
          },
          to: 'tipos_licencias.id'
        }
      }
    }

  };

  /** esquema de validaciones */
  static get jsonSchema() {
    return {
      type: "object",
      additionalProperties: false,
      required: [
        "nombre",
        "descripcion",
      ],
      properties: {
        id: {
          type: "integer",
        },
        nombre: {
          type: "string",
        },
        descripcion: {
          type: "string",
        },
  
      },
    };
  }

}

module.exports = Modulos;
