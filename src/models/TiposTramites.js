const { Model } = require("./model");


class TiposTramites extends Model {
  static get tableName() {
    return "tipos_tramites";
  }

  /** relaciones */
  static get relationMappings() {

    const Tramites = require("./Tramites");

    return {
      tramites: {
        relation: Model.HasManyRelation,
        modelClass: Tramites,
        join: {
          from: "tipos_tramites.id",
          to: "tramites.id_tipo_tramite",
        },
      },
    };
  }

  /** esquema de validaciones */
  static get jsonSchema() {
    return {
      type: "object",
      additionalProperties: false,
      required: [
        "nombre",
        "descripcion",
      ],
      properties: {
        id: {
          type: "integer",
        },
        nombre: {
          type: "string",
        },
        descripcion: {
          type: "string",
        },
  
      },
    };
  }

}

module.exports = TiposTramites;
