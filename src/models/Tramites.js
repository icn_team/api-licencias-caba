const { Model } = require("./model");


class Tramites extends Model {
  static get tableName() {
    return "tramites";
  }

  /** relaciones */
  static get relationMappings() {

    const TiposTramites = require("./TiposTramites");
    const TiposLicencias = require("./TiposLicencias");
    const Certificados = require("./Certificados");
    const AutoTests = require("./AutoTests");
    const users = require("./users");
    const EstadosTramites = require("./EstadosTramites");

    return {
      tipo_tramite: {
        relation: Model.BelongsToOneRelation,
        modelClass: TiposTramites,
        join: {
          from: "tramites.id_tipo_tramite",
          to: "tipos_tramites.id"
        },
      },
      tipos_licencias: {
        relation: Model.ManyToManyRelation,
        modelClass: TiposLicencias,
        join: {
          from: 'tramites.id',
          through: {
            from: 'tramites_tipos_licencias.id_tramite',
            to: 'tramites_tipos_licencias.id_tipo_licencia'
          },
          to: 'tipos_licencias.id'
        }
      },
      estado_tramite: {
        relation: Model.BelongsToOneRelation,
        modelClass: EstadosTramites,
        join: {
          from: "tramites.id_estado_tramite",
          to: "estados_tramites.id",
        },
      },      
      certificados: {
        relation: Model.HasManyRelation,
        modelClass: Certificados,
        join: {
          from: "tramites.id",
          to: "certificados.id_tramite",
        },
      },
      auto_tests: {
        relation: Model.HasManyRelation,
        modelClass: AutoTests,
        join: {
          from: "tramites.id",
          to: "auto_tests.id_tramite",
        },
      },
      usuario: {
        relation: Model.BelongsToOneRelation,
        modelClass: users,
        join: {
          from: "tramites.id_user",
          to: "users.id",
        },
      },
    };

  }


}

module.exports = Tramites;
