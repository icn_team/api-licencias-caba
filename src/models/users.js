const { Model } = require("./model");


class Users extends Model {
  static get tableName() {
    return "users";
  }

  static get relationMappings () {

    const Roles = require("./roles");
    const Tramites = require("./Tramites");

    return {
      roles: {
        relation: Model.ManyToManyRelation,
        modelClass: Roles,
        join: {
          from: 'users.id',
          through: {
            // user_roles es la tabla intermedia.
            from: 'users_roles.id_user',
            to: 'users_roles.id_rol'
          },
          to: 'roles.id'
        }
      },
      tramites: {
        relation: Model.HasManyRelation,
        modelClass: Tramites,
        join: {
          from: "users.id",
          to: "tramites.id_tipo",
        },
      },
    };

  };

  /** esquema de validaciones */
  static get jsonSchema() {
    return {
      type: "object",
      additionalProperties: false,
      required: [
        "nombre",
        "apellido",
        "dni",
        "email",
        "password",
      ],
      properties: {
        id: {
          type: "integer",
        },
        nombre: {
          type: "string",
        },
        apellido: {
          type: "string",
        },
        dni: {
          type: "string",
        },
        email: {
          type: "string",
        },
        password: {
          type: "string",
        },
        // createdAt: {
        //   type: "date-time",
        // },
        // updatedAt: {
        //   type: "date-time",
        // },
      },
    };
  }

}

module.exports = Users;
