const { Model } = require("./model");


class Roles extends Model {
  static get tableName() {
    return "roles";
  }

  static get relationMappings() {
    const Users = require("./users");
    const Permisos = require("./permisos");

    return {

      usuarios: {
        relation: Model.ManyToManyRelation,
        modelClass: Users,
        join: {
          from: 'roles.id',
          through: {
            // user_roles es la tabla intermedia.
            from: 'users_roles.id_rol',
            to: 'users_roles.id_user'
          },
          to: 'users.id'
        }
      },

      permisos: {
        relation: Model.ManyToManyRelation,
        modelClass: Permisos,
        join: {
          from: 'roles.id',
          through: {
            // roles_permisos es la tabla intermedia.
            from: 'roles_permisos.id_rol',
            to: 'roles_permisos.id_permiso'
          },
          to: 'permisos.id'
        }
      }


    }
  };

}

module.exports = Roles;