const { Model } = require("./model");


class TramitesContenidosAvance extends Model {
  static get tableName() {
    return "tramites_contenidos_avance";
  }

  static get idColumn() {
    return 'id_tramite';
  }

  /** esquema de validaciones */
  static get jsonSchema() {
    return {
      type: "object",
      // additionalProperties: false,
      // required: [
      //   "id_tramite",
      //   "id_contenido",
      // ],
      properties: {
        // id_tramite: {
        //   type: "integer",
        // },
        // id_contenido: {
        //   type: "integer",
        // },
        avance: {
          type: ["integer"],
          minimum: 0,
        },
        completado: {
          type:  ["integer"],
          minimum: 0,
        },
        respuesta: {
          type: ["object", "null"],
        }

      }, //properties
    }; //return
  } //jsonSchema

}

module.exports = TramitesContenidosAvance;
