const db = require("../infra/database");
const objection = require("objection");
const moment = require("moment");
//const objectionSoftDelete = require('objection-softdelete');

// const visibilityPlugin = require("objection-visibility").default;
const Model = require("objection").Model;

objection.Model.knex(db);

// Soft delete - https://github.com/ackerdev/objection-softdelete
//objectionSoftDelete.register(objection);

// Visibility plugin - https://github.com/oscaroox/objection-visibility

class GenericModel extends Model {
  $beforeInsert() {
    this.created_at = new Date();
    //this.updated_at = new Date();
    //this.createdAt = new Date().toISOString();
    //this.createdAt = moment().format('YYYY-MM-DD HH:mm:ss');
  }

  $beforeUpdate() {
    this.updated_at = new Date();
    //this.updatedAt = new Date().toISOString();
    //this.updatedAt = moment().format('YYYY-MM-DD HH:mm:ss');
  }
}

module.exports = {
  Model: GenericModel
};
