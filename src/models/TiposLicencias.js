const { Model } = require("./model");


class TiposLicencias extends Model {
  static get tableName() {
    return "tipos_licencias";
  }

  /** relaciones */
  static get relationMappings() {

    const Tramites = require("./Tramites");
    const Modulos = require("./Modulos");
    const Preguntas = require("./Preguntas");

    return {

      tramites: {
        relation: Model.ManyToManyRelation,
        modelClass: Tramites,
        join: {
          from: 'tipos_licencias.id',
          through: {
            from: 'tramites_tipos_licencias.id_tipo_licencia',
            to: 'tramites_tipos_licencias.id_tramite'
          },
          to: 'tramites.id'
        }
      },
      modulos: {
        relation: Model.ManyToManyRelation,
        modelClass: Modulos,
        join: {
          from: 'tipos_licencias.id',
          through: {
            from: 'tipos_licencias_modulos.id_tipo_licencia',
            to: 'tipos_licencias_modulos.id_modulo',
            extra: ['orden']
          },
          to: 'modulos.id'
        }
      },
      preguntas: {
        relation: Model.ManyToManyRelation,
        modelClass: Preguntas,
        join: {
          from: 'tipos_licencias.id',
          through: {
            from: 'preguntas_tipos_licencias.id_tipo_licencia',
            to: 'preguntas_tipos_licencias.id_pregunta'
          },
          to: 'preguntas.id'
        }
      }       
    }

  }

  /** esquema de validaciones */
  static get jsonSchema() {
    return {
      type: "object",
      additionalProperties: false,
      required: [
        "nombre",
        "descripcion",
      ],
      properties: {
        id: {
          type: "integer",
        },
        nombre: {
          type: "string",
        },
        descripcion: {
          type: "string",
        },
      }, //properties
    }; //return
  } //jsonSchema

}

module.exports = TiposLicencias;
