const { Model } = require("./model");


class Permisos extends Model {
  static get tableName() {
    return "permisos";
  }

  static get relationMappings() {
    const Roles = require("./roles");

    return {
      roles: {
        relation: Model.ManyToManyRelation,
        modelClass: Roles,
        join: {
          from: 'permisos.id',
          through: {
            // roles_permisos es la tabla intermedia.
            from: 'roles_permisos.id_permiso',
            to: 'roles_permisos.id_rol'
          },
          to: 'roles.id'
        }
      }
    }
  };

}

module.exports = Permisos;