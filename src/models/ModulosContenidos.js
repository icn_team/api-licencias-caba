const { Model } = require("./model");


class ModulosContenidos extends Model {
  static get tableName() {
    return "modulos_contenidos";
  }

  static get idColumn() {
    return 'id_modulo';
  }

  /** esquema de validaciones */
  static get jsonSchema() {
    return {
      type: "object",
      additionalProperties: false,
      required: [
        "id_modulo",
        "id_contenido",
        "orden",
      ],
      properties: {
        id_modulo: {
          type: "integer",
          minimum: 1,
        },
        id_contenido: {
          type: "integer",
          minimum: 1,
        },
        orden: {
          type: "integer",
        },
      }, //properties
    }; //return
  }

}

module.exports = ModulosContenidos;
