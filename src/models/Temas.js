const { Model } = require("./model");


class Temas extends Model {
  static get tableName() {
    return "temas";
  }

  /** relaciones */
  static get relationMappings() {

    const Preguntas = require("./Preguntas");

    return {
      preguntas: {
        relation: Model.HasManyRelation,
        modelClass: Preguntas,
        join: {
          from: "temas.id",
          to: "preguntas.id_tema",
        },
      },
    };
  }

  /** esquema de validaciones */
  static get jsonSchema() {
    return {
      type: "object",
      additionalProperties: false,
      required: [
        "nombre",
      ],
      properties: {
        id: {
          type: "integer",
        },
        nombre: {
          type: "string",
        }  
      },
    };
  }

}

module.exports = Temas;
