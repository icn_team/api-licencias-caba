const { Model } = require("./model");


class PreguntasTiposLicencias extends Model {
  static get tableName() {
    return "preguntas_tipos_licencias";
  }

  static get idColumn() {
    return 'id_pregunta';
  }

  /** esquema de validaciones */
  static get jsonSchema() {
    return {
      type: "object",
      additionalProperties: false,
      required: [
        "id_pregunta",
        "id_tipo_licencia"
      ],
      properties: {
        id_pregunta: {
          type: "integer",
          minimum: 1,
        },
        id_tipo_licencia: {
          type: "integer",
          minimum: 1,
        },
      }, //properties
    }; //return
  }

}

module.exports = PreguntasTiposLicencias;
