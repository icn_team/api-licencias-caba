const { Model } = require("./model");


class Contenidos extends Model {
  static get tableName() {
    return "contenidos";
  }

  /** relaciones */
  static get relationMappings() {

    const TiposContenidos = require("./TiposContenidos");
    const Modulos = require("./Modulos");

    return {
      tipo_contenido: {
        relation: Model.BelongsToOneRelation,
        modelClass: TiposContenidos,
        join: {
          from: "contenidos.id_tipo_contenido",
          to: "tipos_contenidos.id",
        },
      },
      modulos: {
        relation: Model.ManyToManyRelation,
        modelClass: Modulos,
        join: {
          from: 'contenidos.id',
          through: {
            from: 'modulos_contenidos.id_contenido',
            to: 'modulos_contenidos.id_modulo'
          },
          to: 'modulos.id'
        }
      }
    }


  }

  /** esquema de validaciones */
  static get jsonSchema() {
    return {
      type: "object",
      additionalProperties: false,
      required: [
        "nombre",
        "data",
        "id_tipo_contenido",
      ],
      properties: {
        id: {
          type: "integer",
        },
        nombre: {
          type: "string",
        },
        data: {
          type: "object",
        }, 
        id_tipo_contenido: {
          type: "integer",
          minimum: 1,
        }

      },
    };
  }

}

module.exports = Contenidos;
