const { Model } = require("./model");


class TiposLicenciasModulos extends Model {
  static get tableName() {
    return "tipos_licencias_modulos";
  }

  static get idColumn() {
    return 'id_tipo_licencia';
  }

  /** esquema de validaciones */
  static get jsonSchema() {
    return {
      type: "object",
      additionalProperties: false,
      required: [
        "id_tipo_licencia",
        "id_modulo",
        "orden",
      ],
      properties: {
        id_tipo_licencia: {
          type: "integer",
          minimum: 1,
        },
        id_modulo: {
          type: "integer",
          minimum: 1,
        },
        orden: {
          type: "integer",
        },
      }, //properties
    }; //return
  } //jsonSchema

}

module.exports = TiposLicenciasModulos;
