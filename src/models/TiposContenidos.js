const { Model } = require("./model");


class TiposContenidos extends Model {
  static get tableName() {
    return "tipos_contenidos";
  }

  /** relaciones */
  static get relationMappings() {

    const Contenidos = require("./Contenidos");

    return {
      contenidos: {
        relation: Model.HasManyRelation,
        modelClass: Contenidos,
        join: {
          from: "tipos_contenidos.id",
          to: "contenidos.id_tipo_contenido",
        },
      },
    };
  }

  /** esquema de validaciones */
  static get jsonSchema() {
    return {
      type: "object",
      additionalProperties: false,
      required: [
        "nombre",
      ],
      properties: {
        id: {
          type: "integer",
        },
        nombre: {
          type: "string",
        },
  
      },
    };
  }

}

module.exports = TiposContenidos;
