const { Model } = require("./model");


class Certificados extends Model {
  static get tableName() {
    return "certificados";
  }

  /** relaciones */
  static get relationMappings() {

    const Tramites = require("./Tramites");

    return {
      tramite: {
        relation: Model.BelongsToOneRelation,
        modelClass: Tramites,
        join: {
          from: "certificados.id_tramite",
          to: "tramites.id",
        },
      },
    };

  }

  /** esquema de validaciones */
  static get jsonSchema() {
    return {
      type: "object",
      additionalProperties: false,
      required: [
        "id_tramite",
        "data",
      ],
      properties: {
        id: {
          type: "integer",
        },
        id_tramite: {
          type: "integer",
          minimum: 1,
        },
        data: {
          type: "object",
        } 

      },
    };
  }

}

module.exports = Certificados;
