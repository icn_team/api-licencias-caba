const { Model } = require("./model");


class EstadosAutoTest extends Model {
  static get tableName() {
    return "estados_auto_tests";
  }

  /** relaciones */
  static get relationMappings() {

    const AutoTests = require("./AutoTests");

    return {
      auto_tests: {
        relation: Model.HasManyRelation,
        modelClass: AutoTests,
        join: {
          from: "estados_auto_tests.id",
          to: "auto_test.id_estado",
        },
      },
    }

  }

  /** esquema de validaciones */
  static get jsonSchema() {
    return {
      type: "object",
      additionalProperties: false,
      required: [
        "nombre",
      ],
      properties: {
        id: {
          type: "integer",
        },
        nombre: {
          type: "string",
        },

  
      },
    };
  }

}

module.exports = EstadosAutoTest;
