const { Model } = require("./model");


class AutoTests extends Model {
  static get tableName() {
    return "auto_tests";
  }

  static get idColumn() {
    return 'id';
  }

  /** relaciones */
  static get relationMappings() {

    const AutoTestsPreguntas = require("./AutoTestsPreguntas");
    const Tramites = require("./Tramites");
    const TiposLicencias = require("./TiposLicencias");
    const EstadosAutoTest = require("./EstadosAutoTest");

    return {
      auto_tests_preguntas: {
        relation: Model.HasManyRelation,
        modelClass: AutoTestsPreguntas,
        join: {
          from: "auto_tests.id",
          to: "auto_tests_preguntas.id_auto_test",
        },
      },
      tramite: {
        relation: Model.BelongsToOneRelation,
        modelClass: Tramites,
        join: {
          from: "auto_tests.id_tramite",
          to: "tramites.id",
        },
      },
      tipo_licencia: {
        relation: Model.BelongsToOneRelation,
        modelClass: TiposLicencias,
        join: {
          from: "auto_tests.id_tipo_licencia",
          to: "tipos_licencias.id",
        },
      }, 
      estado: {
        relation: Model.BelongsToOneRelation,
        modelClass: EstadosAutoTest,
        join: {
          from: "auto_tests.id_estado",
          to: "estados_auto_tests.id",
        },
      },            
    };

  }

  /** esquema de validaciones */
  static get jsonSchema() {
    return {
      type: "object",
      additionalProperties: false,
      required: [
      ],
      properties: {
        id: {
          type: "integer", 
        },
        id_tramite: {
          type: "integer",
          minimum: 1,
        },
        id_tipo_licencia: {
          type: "integer",
          minimum: 1,
        },
        id_estado: {
          type: "integer",
          minimum: 1,
        },        
        acertividad: {
          type: "number",
          minimum: 0,
        },
        tiempo_ini: {
          type: "string",
        },         

      },
    };
  }

}

module.exports = AutoTests;
