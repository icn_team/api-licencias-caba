require("dotenv").config();
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const apiMiddleware = require("./middlewares/api");
// const swStats = require("swagger-stats");
// const paginate = require("express-paginate");
const basicauth = require("basicauth-middleware");
const swaggerSetup = require("./docs/api-docs");
const swaggerUi = require("swagger-ui-express");
const helmet = require("helmet");
// const paginateFixed = require("./middlewares/paginateFixed");
var cors = require('cors');

// app.use(paginate.middleware(50, 1000));
app.use(bodyParser.json({ limit: "150mb", type: "application/json" }));
// app.use(paginateFixed.fixPagination);

//app.use(helmet());

app.use(express.static(__dirname + '/public'));

app.options('*', cors());
app.use(cors());


global.__basedir = __dirname;


//Panel de monitoreo de la API - swager-stats
// app.use(swStats.getMiddleware({}));

//api-documantation
app.use("/api-docs", 
[basicauth("desarrollo", "123654"), swaggerUi.serve],
  swaggerUi.setup(swaggerSetup, {
    customCss: ".swagger-ui .topbar { display: none }",
  })
);

//retorna fotos
app.get("/autotests-foto/:id", function (req, res) {

  var foto_id = req.params.id;
  var ruta_foto = '';

  if(foto_id > 0 && foto_id < 10) //1 a 9
  {
    ruta_foto = '00'+foto_id+'/'+'00'+foto_id+'-H.png';
  }
  if(foto_id >= 10 && foto_id < 100) //1 a 99
  {
    ruta_foto = '0'+foto_id+'/'+'0'+foto_id+'-H.png';
  }
  if(foto_id >= 100) //100 a 99999999
  {
    ruta_foto = foto_id+'/'+foto_id+'-H.png';
  }
 
  res.sendFile(__dirname + '/public/img/auto_test/'+ruta_foto);
});

// Routes
const authRoutes = require("./interfaces/http/modules/auth/router");
//const importaExcelRoutes = require("./interfaces/http/modules/importaExcel/router");
const tiposTramitesRoutes = require("./interfaces/http/modules/tiposTramites/router");
const temasRoutes = require("./interfaces/http/modules/temas/router");
const modulosRoutes = require("./interfaces/http/modules/modulos/router");
const tiposLicenciasRoutes = require("./interfaces/http/modules/tiposLicencias/router");
const tramitesRoutes = require("./interfaces/http/modules/tramites/router");
const contenidosRoutes = require("./interfaces/http/modules/contenidos/router");
//const preguntasRoutes = require("./interfaces/http/modules/preguntas/router");
const autoTestsRoutes = require("./interfaces/http/modules/autoTests/router");
const autoTestsPreguntasRoutes = require("./interfaces/http/modules/autoTestsPreguntas/router");
const scormRoutes = require("./interfaces/http/modules/scorm/router");
const imagenesRoutes = require("./interfaces/http/modules/imagenes/router");

app.use("/api/v1.0.0", authRoutes);
//app.use("/api/v1.0.0", importaExcelRoutes);
app.use("/api/v1.0.0", tiposTramitesRoutes);
app.use("/api/v1.0.0", temasRoutes);
app.use("/api/v1.0.0", modulosRoutes);
app.use("/api/v1.0.0", tiposLicenciasRoutes);
app.use("/api/v1.0.0", tramitesRoutes);
app.use("/api/v1.0.0", contenidosRoutes);
//app.use("/api/v1.0.0", preguntasRoutes);
app.use("/api/v1.0.0", autoTestsRoutes);
app.use("/api/v1.0.0", autoTestsPreguntasRoutes);
app.use("/api/v1.0.0", scormRoutes);
app.use("/api/v1.0.0", imagenesRoutes);


//pruebas
// const db = require("../src/infra/database/index");
// app.get('/test', async (req, res) => {
// try{
//     response = await db.raw(`
//                               SELECT p.id as pregunta_id,
//                                       t.id as tema_id,
//                                       t.nombre as tema,
//                                       p.nombre as pregunta 
//                               from temas t
//                               inner join preguntas p on t.id = p.id_tema
//                             `);
//     res.status(200).send(response[0]);
// }
// catch(ex)
// {
//   res.status(500).send(ex);
// }

// });





//Error 400
app.use((req, res, next) => {
  res.status(400).send({ code: 400, message: "Petición inválida" });
});

//Otros errores
app.use(apiMiddleware.addErrors);

module.exports = app;
