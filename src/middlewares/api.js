"use strict";
const objection = require("objection");
const UserException = require("../helpers/UserException");
const CustomException = require("../helpers/CustomException");

exports.addHeaders = function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method"
  );
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE");
  res.header("Content-Type", "application/json");

  next();
};

exports.addErrors = function (err, req, res, next) {
  try {
    console.error(err);

    if (err instanceof objection.ValidationError) {
      res.status(400).json({
        code: 400,
        message: err.message,
        error: err.data,
      });
    } else if (err instanceof objection.NotFoundError) {
      res.status(404).json({
        code: 404,
        message: err.message,
        error: err.data,
      });
    } else if (err instanceof CustomException) {
      res.status(400).json({
        status : err.status,
        developer_message : err.developerMessage,
        user_message : err.userMessage,
        error_code : err.errorCode,
        more_info : err.moreInfo,
      });
    } else if (err instanceof UserException) {
      res.status(400).json({
        code: err.code,
        message: err.message,
        // error: err.message,
      });
    } else {
      if (err.code == "23505") {
        return res.status(400).send({
          code: 400,
          message: "Elemento repetido. No se puede ingresar múltiples veces.",
        });
      }

      var response = {
        code: 500,
        message: err.message || "Internal server error",
      };

      if (err.error) {
        response.error = err.error;
      }

      res.status(response.code).send(response);
    }
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: "Internal server error",
    });
  }
};
