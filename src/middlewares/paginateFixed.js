"use strict";

exports.fixPagination = function(req, res, next) {
  // Resto una página, ya que Knex cuenta a partir de 0

  if (req.query.page) {
    req.query.page -= 1;
  }

  if (req.query.limit == 0) {
    req.query.limit = 1000000;
  }

  next();
};
