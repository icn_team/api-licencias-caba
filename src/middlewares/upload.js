const multer = require("multer");

const zipFilter = (req, file, cb) => {
  cb(null, true);
  // if (file.mimetype == 'application/zip') {
  //   cb(null, true);
  // } else {
  //   //cb("Please upload only .zip", false);
  //   cb(null, false);
  //   cb(new Error('Por favor cargue solamente archivos .zip correspondientes al scorm'));
  // }
};

var storage = multer.diskStorage({

    destination: function (req, file, cb) {
        cb(null, __basedir + "/../uploads/");
      },
    filename: function (req, file, cb) {
        cb(null, sanea_string(`${Date.now()}-scorm-${file.originalname}`));
    }
});


String.prototype.replaceAll = function(str1, str2, ignore) 
{
    return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),(ignore?"gi":"g")),(typeof(str2)=="string")?str2.replace(/\$/g,"$$$$"):str2);
}

function sanea_string(text){

	text = text.trim();
	text = text.toLowerCase();

	text = text.replaceAll('¨', '');
	text = text.replaceAll('º', '');
	text = text.replaceAll('~', '');
	text = text.replaceAll('#', '');
	text = text.replaceAll('@', '');
	text = text.replaceAll('|', '');
	text = text.replaceAll('!', '');

	text = text.replaceAll('$', '');
	text = text.replaceAll('%', '');
	text = text.replaceAll('&', '');
	text = text.replaceAll('/', '');
	text = text.replaceAll('(', '');
	text = text.replaceAll(')', '');
	text = text.replaceAll('?', '');
	text = text.replaceAll("'", '');

	text = text.replaceAll('¡', '');
	text = text.replaceAll('¿', '');
	text = text.replaceAll('[', '');
	text = text.replaceAll('^', '');
	text = text.replaceAll('`', '');
	text = text.replaceAll(']', '');
	text = text.replaceAll('+', '');

	text = text.replaceAll('{', '');
	text = text.replaceAll('}', '');
	text = text.replaceAll('¨', '');
	text = text.replaceAll('´', '');
	text = text.replaceAll('<', '');
	text = text.replaceAll('>', '');
	text = text.replaceAll(';', '');
	text = text.replaceAll(',', '');
	text = text.replaceAll(':', '');
	text = text.replaceAll(' ', '');

	text = text.replaceAll('à', 'a');
	text = text.replaceAll('ä', 'a');
	text = text.replaceAll('â', 'a');
	text = text.replaceAll('ª', 'a');

	text = text.replaceAll('è', 'e');
	text = text.replaceAll('ë', 'e');
	text = text.replaceAll('ê', 'e');

	text = text.replaceAll('ì', 'i');
	text = text.replaceAll('ï', 'i');
	text = text.replaceAll('î', 'i');

	text = text.replaceAll('ò', 'o');
	text = text.replaceAll('ö', 'o');
	text = text.replaceAll('ô', 'o');

	text = text.replaceAll('ù', 'u');
	text = text.replaceAll('ü', 'u');
	text = text.replaceAll('û', 'u');

	text = text.replaceAll('ñ', 'n');
	text = text.replaceAll('ç', 'c');

	return text;

}

var upload = multer({ storage: storage, fileFilter: zipFilter });
module.exports = upload;