/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex('tipos_contenidos').del()
  await knex('tipos_contenidos').insert([
    {id: 1, nombre: 'Guia de lectura con video'},
    {id: 2, nombre: 'Contenido interactivo'},
    {id: 3, nombre: 'Actividad'},
  ]);
};
