/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex('temas').del()
  await knex('temas').insert([
    {id: 1, nombre: 'Generalidades'},
    {id: 2, nombre: 'Prioridades de Paso'},
    {id: 3, nombre: 'Velocidad'},
    {id: 4, nombre: 'Capacidad Natural'},
    {id: 5, nombre: 'Estacionamiento'},
    {id: 6, nombre: 'Elementos de Seguridad'},
    {id: 7, nombre: 'Situaciones Adversas'},
    {id: 8, nombre: 'Legales y Documentación'},
    {id: 9, nombre: 'Señales'},
    {id: 10, nombre:'De relevancia'},

  ]);
};
