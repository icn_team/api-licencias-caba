/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex('estados_auto_tests').del()
  await knex('estados_auto_tests').insert([
    {id: 1, nombre: 'No Iniciado'},
    {id: 2, nombre: 'En Curso'},
    {id: 3, nombre: 'Finalizado'},
  ]);
};
