/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex('estados_tramites').del()
  await knex('estados_tramites').insert([
    {id: 1, nombre: 'En Curso'},
    {id: 2, nombre: 'Finalizado'},
  ]);
};
