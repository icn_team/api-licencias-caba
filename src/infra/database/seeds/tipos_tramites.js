/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex('tipos_tramites').del()
  await knex('tipos_tramites').insert([
    {id: 1, nombre: 'tipo tramite 1', descripcion : 'descripcion tipo tramite 1'},
    {id: 2, nombre: 'tipo tramite 2', descripcion : 'descripcion tipo tramite 2'}

  ]);
};
