"use strict";
require("dotenv").config();
var env = process.env.NODE_ENV || "development";

const config = require("../../../knexfile");
const database = require("knex")(config[env]);

module.exports = database;
