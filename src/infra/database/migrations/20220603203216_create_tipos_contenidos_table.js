
exports.up = function(knex) {
  return knex.schema.createTable("tipos_contenidos", (table) => {
    table.increments("id").primary().unsigned().notNullable();
    table.string("nombre").notNullable();

    table.datetime("created_at").nullable();
    table.datetime("updated_at").nullable();
    table.datetime("deleted_at").nullable();
    
    table.engine('InnoDB');
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable("tipos_contenidos");
};
