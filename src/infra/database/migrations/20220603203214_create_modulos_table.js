
exports.up = function(knex) {
  return knex.schema.createTable("modulos", (table) => {
    table.increments("id").primary().unsigned().notNullable();
    table.string("nombre").notNullable();
    table.text("descripcion").notNullable();

    table.datetime("created_at").nullable();
    table.datetime("updated_at").nullable();
    table.datetime("deleted_at").nullable();

    table.engine('InnoDB');
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable("modulos");
};
