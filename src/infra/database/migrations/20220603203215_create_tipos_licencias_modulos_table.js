
exports.up = function(knex) {
  return knex.schema.createTable("tipos_licencias_modulos", (table) => {
    table.integer("id_tipo_licencia").unsigned().notNullable();
    table.integer("id_modulo").unsigned().notNullable();

    table.datetime("created_at").nullable();
    table.datetime("updated_at").nullable();
    table.datetime("deleted_at").nullable();
    
    table.engine('InnoDB');

    table.foreign("id_tipo_licencia").references("id").inTable("tipos_licencias").onUpdate("cascade");
    table.foreign("id_modulo").references("id").inTable("modulos").onUpdate("cascade");
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable("tipos_licencias_modulos");
};
