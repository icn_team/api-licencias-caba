
exports.up = function(knex) {
  return knex.schema.createTable("modulos_contenidos", (table) => {
    table.integer("id_modulo").unsigned().notNullable();
    table.integer("id_contenido").unsigned().notNullable();

    table.datetime("created_at").nullable();
    table.datetime("updated_at").nullable();
    table.datetime("deleted_at").nullable();

    table.engine('InnoDB');

    table.foreign("id_modulo").references("id").inTable("modulos").onUpdate("cascade");
    table.foreign("id_contenido").references("id").inTable("contenidos").onUpdate("cascade");
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable("modulos_contenidos");
};
