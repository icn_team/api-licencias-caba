
exports.up = function(knex) 
{


  return (
    knex.schema
      .raw("ALTER TABLE auto_tests DROP FOREIGN KEY auto_tests_id_tramite_foreign")
      .raw('ALTER TABLE auto_tests ADD CONSTRAINT auto_tests_id_tramite_foreign FOREIGN KEY (id_tramite) REFERENCES tramites (id) ON DELETE CASCADE ON UPDATE CASCADE')

      .raw("ALTER TABLE auto_tests_preguntas DROP FOREIGN KEY auto_tests_preguntas_id_auto_test_foreign")
      .raw('ALTER TABLE auto_tests_preguntas ADD CONSTRAINT auto_tests_preguntas_id_auto_test_foreign FOREIGN KEY (id_auto_test) REFERENCES auto_tests (id) ON DELETE CASCADE ON UPDATE CASCADE')

      .raw("ALTER TABLE tramites_contenidos_avance DROP FOREIGN KEY tramites_contenidos_avance_id_tramite_foreign")
      .raw('ALTER TABLE tramites_contenidos_avance ADD CONSTRAINT tramites_contenidos_avance_id_tramite_foreign FOREIGN KEY (id_tramite) REFERENCES tramites (id) ON DELETE CASCADE ON UPDATE CASCADE')

      .raw("ALTER TABLE tramites_tipos_licencias DROP FOREIGN KEY tramites_tipos_licencias_id_tramite_foreign")
      .raw('ALTER TABLE tramites_tipos_licencias ADD CONSTRAINT tramites_tipos_licencias_id_tramite_foreign FOREIGN KEY (id_tramite) REFERENCES tramites (id) ON DELETE CASCADE ON UPDATE CASCADE')

      .raw("ALTER TABLE certificados DROP FOREIGN KEY certificados_id_tramite_foreign")
      .raw('ALTER TABLE certificados ADD CONSTRAINT certificados_id_tramite_foreign FOREIGN KEY (id_tramite) REFERENCES tramites (id) ON DELETE CASCADE ON UPDATE CASCADE')

    );
};

exports.down = function(knex) {


  return (
    knex.schema
      .raw("ALTER TABLE auto_tests DROP FOREIGN KEY auto_tests_id_tramite_foreign")
      .raw('ALTER TABLE auto_tests ADD CONSTRAINT auto_tests_id_tramite_foreign FOREIGN KEY (id_tramite) REFERENCES tramites (id) ON DELETE RESTRICT ON UPDATE CASCADE')

      .raw("ALTER TABLE auto_tests_preguntas DROP FOREIGN KEY auto_tests_preguntas_id_auto_test_foreign")
      .raw('ALTER TABLE auto_tests_preguntas ADD CONSTRAINT auto_tests_preguntas_id_auto_test_foreign FOREIGN KEY (id_auto_test) REFERENCES auto_tests (id) ON DELETE RESTRICT ON UPDATE CASCADE')

      .raw("ALTER TABLE tramites_contenidos_avance DROP FOREIGN KEY tramites_contenidos_avance_id_tramite_foreign")
      .raw('ALTER TABLE tramites_contenidos_avance ADD CONSTRAINT tramites_contenidos_avance_id_tramite_foreign FOREIGN KEY (id_tramite) REFERENCES tramites (id) ON DELETE RESTRICT ON UPDATE CASCADE')

      .raw("ALTER TABLE tramites_tipos_licencias DROP FOREIGN KEY tramites_tipos_licencias_id_tramite_foreign")
      .raw('ALTER TABLE tramites_tipos_licencias ADD CONSTRAINT tramites_tipos_licencias_id_tramite_foreign FOREIGN KEY (id_tramite) REFERENCES tramites (id) ON DELETE RESTRICT ON UPDATE CASCADE')

      .raw("ALTER TABLE certificados DROP FOREIGN KEY certificados_id_tramite_foreign")
      .raw('ALTER TABLE certificados ADD CONSTRAINT certificados_id_tramite_foreign FOREIGN KEY (id_tramite) REFERENCES tramites (id) ON DELETE RESTRICT ON UPDATE CASCADE')

    );

};
