
exports.up = function(knex) {
  return knex.schema.createTable("roles_permisos", (table) => {
    table.integer("id_rol").unsigned().notNullable();
    table.integer("id_permiso").unsigned().notNullable();

    table.datetime("created_at").nullable();
    table.datetime("updated_at").nullable();
    table.datetime("deleted_at").nullable();
    
    table.engine('InnoDB');

    table.foreign("id_rol").references("id").inTable("roles").onUpdate("cascade");
    table.foreign("id_permiso").references("id").inTable("permisos").onUpdate("cascade");
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable("roles_permisos");
};
