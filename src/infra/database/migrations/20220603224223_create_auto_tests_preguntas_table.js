
exports.up = function(knex) {
  return knex.schema.createTable("auto_tests_preguntas", (table) => {
    table.bigInteger("id_auto_test").unsigned().notNullable();
    table.integer("id_pregunta").unsigned().notNullable();
    table.integer("rta").nullable();
    table.jsonb("data_copia").notNullable();
    table.engine('InnoDB');

    table.datetime("created_at").nullable();
    table.datetime("updated_at").nullable();
    table.datetime("deleted_at").nullable();

    table.foreign("id_auto_test").references("id").inTable("auto_tests").onUpdate("cascade");
    // table.foreign("id_pregunta").references("id").inTable("preguntas").onUpdate("cascade");
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable("auto_tests_preguntas");
};
