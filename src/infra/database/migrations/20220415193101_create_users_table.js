
exports.up = function(knex) {
  return knex.schema.createTable("users", (table) => {
    table.bigIncrements("id").primary().unsigned().notNullable();
    table.string("nombre").notNullable();
    table.string("apellido").notNullable();
    table.string("dni").notNullable();
    table.string("email").notNullable();
    table.string("password").notNullable();
    table.datetime("created_at").nullable();
    table.datetime("updated_at").nullable();
    table.datetime("deleted_at").nullable();

    table.index("dni");
    table.unique("dni");
    table.unique("email");
    table.engine('InnoDB');
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable("users");
};
