
exports.up = function(knex) {
  return knex.schema.createTable("contenidos", (table) => {
    table.increments("id").primary().unsigned().notNullable();
    table.string("nombre").notNullable();
    table.jsonb("data").notNullable();
    table.integer("id_tipo_contenido").unsigned().notNullable();

    table.datetime("created_at").nullable();
    table.datetime("updated_at").nullable();
    table.datetime("deleted_at").nullable();
    
    table.engine('InnoDB');

    table.foreign("id_tipo_contenido").references("id").inTable("tipos_contenidos").onUpdate("cascade");
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable("contenidos");
};
