
exports.up = function(knex) {
  return knex.schema.createTable("preguntas", (table) => {
    table.increments("id").primary().unsigned().notNullable();
    table.integer("id_tema").unsigned().notNullable();
    table.jsonb("data").notNullable();

    table.datetime("created_at").nullable();
    table.datetime("updated_at").nullable();
    table.datetime("deleted_at").nullable();

    table.engine('InnoDB');

    table.foreign("id_tema").references("id").inTable("temas").onUpdate("cascade");
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable("preguntas");
};
