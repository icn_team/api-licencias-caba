
exports.up = function(knex) {
  return knex.schema.createTable("users_roles", (table) => {
    table.bigInteger("id_user").unsigned().notNullable();
    table.integer("id_rol").unsigned().notNullable();

    table.datetime("created_at").nullable();
    table.datetime("updated_at").nullable();
    table.datetime("deleted_at").nullable();
    
    table.engine('InnoDB');

    table.foreign("id_user").references("id").inTable("users").onUpdate("cascade");
    table.foreign("id_rol").references("id").inTable("roles").onUpdate("cascade");
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable("users_roles");
};
