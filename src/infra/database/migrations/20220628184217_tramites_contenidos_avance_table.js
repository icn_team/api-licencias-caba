
exports.up = function(knex) {
  return knex.schema.createTable("tramites_contenidos_avance", (table) => {
    table.bigInteger("id_tramite").unsigned().notNullable();
    table.integer("id_contenido").unsigned().notNullable();
    table.integer("avance").unsigned().notNullable();
    table.integer("completado").unsigned().notNullable();
    table.jsonb("respuesta").nullable();

    table.datetime("created_at").nullable();
    table.datetime("updated_at").nullable();
    table.datetime("deleted_at").nullable();

    table.engine('InnoDB');

    table.foreign("id_tramite").references("id").inTable("tramites").onUpdate("cascade");
    table.foreign("id_contenido").references("id").inTable("contenidos").onUpdate("cascade");
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable("tramites_contenidos_avance");
};
