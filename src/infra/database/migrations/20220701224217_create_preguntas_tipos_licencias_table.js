
exports.up = function(knex) {
  return knex.schema.createTable("preguntas_tipos_licencias", (table) => {
    table.integer("id_pregunta").unsigned().notNullable();
    table.integer("id_tipo_licencia").unsigned().notNullable();

    table.datetime("created_at").nullable();
    table.datetime("updated_at").nullable();
    table.datetime("deleted_at").nullable();

    table.engine('InnoDB');

    table.foreign("id_pregunta").references("id").inTable("preguntas").onUpdate("cascade");
    table.foreign("id_tipo_licencia").references("id").inTable("tipos_licencias").onUpdate("cascade");
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable("preguntas_tipos_licencias");
};
