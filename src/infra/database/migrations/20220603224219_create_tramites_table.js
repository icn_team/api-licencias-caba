
exports.up = function(knex) {
  return knex.schema.createTable("tramites", (table) => {
    table.bigIncrements("id").primary().unsigned().notNullable();
    table.integer("id_tipo_tramite").unsigned().notNullable();
    table.bigInteger("id_user").unsigned().notNullable();
    table.integer("id_estado_tramite").unsigned().notNullable();
    table.datetime("fecha_curso_sincrono").nullable();
    table.integer("presente_curso_sincrono").unsigned().nullable();
    table.integer("calificacion_curso_sincrono").unsigned().nullable();

    table.datetime("created_at").nullable();
    table.datetime("updated_at").nullable();
    table.datetime("deleted_at").nullable();

    table.engine('InnoDB');

    table.foreign("id_tipo_tramite").references("id").inTable("tipos_tramites").onUpdate("cascade");
    table.foreign("id_user").references("id").inTable("users").onUpdate("cascade");
    table.foreign("id_estado_tramite").references("id").inTable("estados_tramites").onUpdate("cascade");
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable("tramites");
};
