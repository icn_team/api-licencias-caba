
exports.up = function(knex) {
  return knex.schema.createTable("certificados", (table) => {
    table.bigIncrements("id").primary().unsigned().notNullable();
    table.bigInteger("id_tramite").unsigned().notNullable();
    table.jsonb("data").notNullable();

    table.datetime("created_at").nullable();
    table.datetime("updated_at").nullable();
    table.datetime("deleted_at").nullable();

    table.engine('InnoDB');

    table.foreign("id_tramite").references("id").inTable("tramites").onUpdate("cascade");
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable("certificados");
};
