
exports.up = function(knex) {
  return knex.schema.createTable("tramites_tipos_licencias", (table) => {
    table.bigInteger("id_tramite").unsigned().notNullable();
    table.integer("id_tipo_licencia").unsigned().notNullable();

    table.datetime("created_at").nullable();
    table.datetime("updated_at").nullable();
    table.datetime("deleted_at").nullable();

    table.engine('InnoDB');

    table.foreign("id_tramite").references("id").inTable("tramites").onUpdate("cascade");
    table.foreign("id_tipo_licencia").references("id").inTable("tipos_licencias").onUpdate("cascade");
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable("tramites_tipos_licencias");
};
