
exports.up = function(knex) {
  return knex.schema.raw(`
    ALTER TABLE tipos_licencias_modulos ADD COLUMN orden integer not null;
  `); 
};

exports.down = function(knex) {
  return knex.schema.raw(`
    ALTER TABLE tipos_licencias_modulos DROP COLUMN orden;
  `);
};

