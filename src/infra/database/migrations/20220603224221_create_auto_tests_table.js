
exports.up = function(knex) {
  return knex.schema.createTable("auto_tests", (table) => {
    table.bigIncrements("id").primary().unsigned().notNullable();
    table.bigInteger("id_tramite").unsigned().notNullable();
    table.integer("id_tipo_licencia").unsigned().notNullable();
    table.integer("id_estado").unsigned().notNullable();
    table.datetime("time_ini").nullable();
    table.float("acertividad").notNullable();

    table.datetime("created_at").nullable();
    table.datetime("updated_at").nullable();
    table.datetime("deleted_at").nullable();

    table.engine('InnoDB');

    table.foreign("id_tramite").references("id").inTable("tramites").onUpdate("cascade");
    table.foreign("id_tipo_licencia").references("id").inTable("tipos_licencias").onUpdate("cascade");
    table.foreign("id_estado").references("id").inTable("estados_auto_tests").onUpdate("cascade");    
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable("auto_tests");
};
