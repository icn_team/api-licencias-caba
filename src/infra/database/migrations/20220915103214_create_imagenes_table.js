
exports.up = function(knex) {
  return knex.schema.createTable("imagenes", (table) => {
    table.increments("id").primary().unsigned().notNullable();
    table.string("nombre").nullable();
    table.text("descripcion").nullable();
    table.string("ruta").notNullable();

    table.datetime("created_at").nullable();
    table.datetime("updated_at").nullable();
    table.datetime("deleted_at").nullable();

    table.engine('InnoDB');
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable("imagenes");
};
