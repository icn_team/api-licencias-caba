
exports.up = function(knex) {
  return knex.schema.raw(`
    ALTER TABLE modulos_contenidos ADD COLUMN orden integer not null;
  `); 
};

exports.down = function(knex) {
  return knex.schema.raw(`
    ALTER TABLE modulos_contenidos DROP COLUMN orden;
  `);
};

