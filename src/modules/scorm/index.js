module.exports = {
  index: require("./actions/index"),
  show: require("./actions/show"),
  update: require("./actions/update"),
  destroy: require("./actions/destroy"),
  upload: require("./actions/upload"), 
};
  