const scorms = require("../../../models/Scorms");
const CustomException = require("../../../helpers/CustomException");
const fs = require("fs");

const destroy = async (id) => {
  try {

    let scorm = await scorms.query().findById(id);

    if(!scorm)
      throw new CustomException(400, "Id de scorm invalido","scorm inexistente.");


    let numberOfDeletedRows = await scorms.query().deleteById(id);

    // if(numberOfDeletedRows == 0)
    //   throw new CustomException(400, "Id de scorm invalido","scorm inexistente.");

    const pathToFile = __basedir + "/public/paquetes_scorms/"+scorm.ruta;

    fs.rmdir(pathToFile, { recursive: true }, err => {
      if (err) {
        throw err
      }
    })

    // fs.unlink(pathToFile, function(err) {
    //   if (err) {
    //     throw err
    //   } else {
    //     //console.log("Successfully deleted the file.")
    //   }
    // })

    return numberOfDeletedRows;
  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = destroy;