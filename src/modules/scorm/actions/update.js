const scorms = require("../../../models/Scorms");
const CustomException = require("../../../helpers/CustomException");
const fs = require("fs");
var AdmZip = require("adm-zip");
const Ajv = require('ajv');
const ajv = new Ajv();
const schema = require("../../../schemas/scorms/update");
const validateRequest = ajv.compile(schema);

const update = async (id,datos) => {

  try {
    //si manda scorm, lo actualiza, sino deja el que estaba

    const nombre = datos.body.nombre;
    const descripcion = datos.body.descripcion;

    if (!validateRequest(datos))
      throw new CustomException(400, validateRequest.errors,"Algunos datos ingresados son inválidos.");

    if(datos.file) //actualizo scorm y el resto
    {
      //borro la que tenia fisicamente
      let scorm_old = await scorms.query().findById(id);
      if(!scorm_old)
        throw new CustomException(400, "Id de scorm invalido","scorm inexistente.");

      const pathToFile = __basedir + "/public/paquetes_scorms/"+scorm_old.ruta;
      fs.rmdir(pathToFile, { recursive: true }, err => {
        if (err) {
          throw err
        }
      })

      //fin borro la que tenia fisicamente

      const ruta_zip = datos.file.filename;
      const ruta = ruta_zip.replace(".zip","");

      const scorm = await scorms.query().updateAndFetchById(id, {
        nombre, 
        descripcion,
        ruta
      });


      //deszipea y extrae
      var zip = new AdmZip("./uploads/"+ruta_zip);
      zip.extractAllTo(/*target path*/ __basedir + '/public/paquetes_scorms/'+ruta, /*overwrite*/ true);
     
      //elimina zip
      const pathToFile2 = __basedir + "/../uploads/"+ruta_zip;
      fs.unlink(pathToFile2, function(err) {
        if (err) {
          throw err
        } else {
          //console.log("Successfully deleted the file.")
        }
      })


      return scorm;
    }
    else //sin scorm, solo actualizo nombre y descripcion
    {
      const scorm = await scorms.query().updateAndFetchById(id, {
        nombre, 
        descripcion,
      });

      if(!scorm)
        throw new CustomException(400, "Id de scorm invalido","scorm inexistente.");

      return scorm;
    } 

  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = update;