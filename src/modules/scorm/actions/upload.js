const scorms = require("../../../models/Scorms");
const CustomException = require("../../../helpers/CustomException");
const fs = require("fs");
//para deszipear archivos scorm
var AdmZip = require("adm-zip");
const Ajv = require('ajv');
const ajv = new Ajv();
const schema = require("../../../schemas/scorms/insert");
const validateRequest = ajv.compile(schema);

const upload = async (datos) => {

  try {
    const nombre = datos.body.nombre;
    const descripcion = datos.body.descripcion;
    const ruta_zip = datos.file.filename;
    const ruta = ruta_zip.replace(".zip","");

    if (!validateRequest(datos))
      throw new CustomException(400, validateRequest.errors,"Algunos datos ingresados son inválidos.");

    const scorm = await scorms.query().insertAndFetch({
      nombre, 
      descripcion,
      ruta
    });

    var zip = new AdmZip("./uploads/"+ruta_zip);
    zip.extractAllTo(/*target path*/ __basedir + '/public/paquetes_scorms/'+ruta, /*overwrite*/ true);
    const pathToFile = __basedir + "/../uploads/"+ruta_zip;

    fs.unlink(pathToFile, function(err) {
      if (err) {
        throw err
      } else {
        //console.log("Successfully deleted the file.")
      }
    })

    return scorm;
  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = upload;