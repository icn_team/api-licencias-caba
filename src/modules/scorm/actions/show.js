const scorms = require("../../../models/Scorms");
const CustomException = require("../../../helpers/CustomException");

const show = async (id) => {
  try {
    let scorm = await scorms.query().findById(id);

    if(!scorm)
      throw new CustomException(400, "Id de scorm invalido","scorm inexistente.");

    return scorm;

  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = show;