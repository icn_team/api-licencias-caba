const imagenes = require("../../../models/Imagenes");
const filtros = require("../../../helpers/Filtros");

const index = async (req) => {
  try {

    const {limit,offset,order,sort} = filtros(req);

    const query = imagenes.query();

    if (req.query.q && req.query.q != '') {
      query.orWhere('imagenes.nombre', 'like', '%'+req.query.q+'%')
      query.orWhere('imagenes.descripcion', 'like', '%'+req.query.q+'%')
    }

    const [cantidad, results] = await Promise.all([
      query.resultSize(),
      query.offset(offset).limit(limit).orderBy(order,sort)
    ]);

    var resultset = {"count":cantidad,"offset":offset,"limit":limit};
    var metadata = {resultset};

    return {
      metadata,
      results
    }
  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = index;
