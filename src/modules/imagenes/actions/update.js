const imagenes = require("../../../models/Imagenes");
const CustomException = require("../../../helpers/CustomException");
const fs = require("fs");

const Ajv = require('ajv');
const ajv = new Ajv();
const schema = require("../../../schemas/imagenes/update");
const validateRequest = ajv.compile(schema);

const update = async (id,datos) => {

  try {
    
    //si manda foto la actualiza, sino deja la que estaba

    const nombre = datos.body.nombre;
    const descripcion = datos.body.descripcion;

    if (!validateRequest(datos))
      throw new CustomException(400, validateRequest.errors,"Algunos datos ingresados son inválidos.");

    if(datos.file) //actualizo imagen y el resto
    {
      //borro la que tenia fisicamente
      let imagen_old = await imagenes.query().findById(id);
      if(!imagen_old)
        throw new CustomException(400, "Id de imagen invalido","imagen inexistente.");

      const pathToFile = __basedir + "/public/imagenes/"+imagen_old.ruta;
      fs.unlink(pathToFile, function(err) {
        if (err) {
          throw err
        } else {
          //console.log("Successfully deleted the file.")
        }
      })
      //fin borro la que tenia fisicamente

      const ruta = datos.file.filename;

      const imagen = await imagenes.query().updateAndFetchById(id, {
        nombre, 
        descripcion,
        ruta
      });

      if(!imagen)
        throw new CustomException(400, "Id de imagen invalido","imagen inexistente.");

      return imagen;
    }
    else //sin foto, solo actualizo nombre y descripcion
    {
      const imagen = await imagenes.query().updateAndFetchById(id, {
        nombre, 
        descripcion,
      });

      if(!imagen)
        throw new CustomException(400, "Id de imagen invalido","imagen inexistente.");

      return imagen;
    } 

    
  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = update;