const imagenes = require("../../../models/Imagenes");
const CustomException = require("../../../helpers/CustomException");
const fs = require("fs");

const destroy = async (id) => {
  try {

    let imagen = await imagenes.query().findById(id);

    if(!imagen)
      throw new CustomException(400, "Id de imagen invalido","imagen inexistente.");


    let numberOfDeletedRows = await imagenes.query().deleteById(id);

    // if(numberOfDeletedRows == 0)
    //   throw new CustomException(400, "Id de imagen invalido","imagen inexistente.");

    const pathToFile = __basedir + "/public/imagenes/"+imagen.ruta;

    // fs.rmdir(pathToFile, { recursive: true }, err => {
    //   if (err) {
    //     throw err
    //   }
    // })

    fs.unlink(pathToFile, function(err) {
      if (err) {
        throw err
      } else {
        //console.log("Successfully deleted the file.")
      }
    })

    return numberOfDeletedRows;
  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = destroy;