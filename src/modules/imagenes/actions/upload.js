const imagenes = require("../../../models/Imagenes");
const CustomException = require("../../../helpers/CustomException");

const fs = require("fs");

const Ajv = require('ajv');
const ajv = new Ajv();
const schema = require("../../../schemas/imagenes/insert");
const validateRequest = ajv.compile(schema);

const upload = async (datos) => {

  try {

    const nombre = datos.body.nombre;
    const descripcion = datos.body.descripcion;
    const ruta = datos.file.filename;

    if (!validateRequest(datos))
      throw new CustomException(400, validateRequest.errors,"Algunos datos ingresados son inválidos.");

    const imagen = await imagenes.query().insertAndFetch({
      nombre, 
      descripcion,
      ruta
    });

    // var zip = new AdmZip("./uploads/"+ruta_zip);

    // zip.extractAllTo(/*target path*/ __basedir + '/public/imagenes/'+ruta, /*overwrite*/ true);

    // const pathToFile = __basedir + "/public/imagenes/"+ruta;

    // fs.unlink(pathToFile, function(err) {
    //   if (err) {
    //     throw err
    //   } else {
    //     //console.log("Successfully deleted the file.")
    //   }
    // })

    return imagen;
  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = upload;