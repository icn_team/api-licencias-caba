const imagenes = require("../../../models/Imagenes");
const CustomException = require("../../../helpers/CustomException");

const show = async (id) => {
  try {
    let imagen = await imagenes.query().findById(id);

    if(!imagen)
      throw new CustomException(400, "Id de imagen invalido","imagen inexistente.");

    return imagen;

  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = show;