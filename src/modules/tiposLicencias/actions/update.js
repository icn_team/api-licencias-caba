const { transaction } = require("objection");
const tiposLicencias = require("../../../models/TiposLicencias");
const tiposLicenciasModulos = require("../../../models/TiposLicenciasModulos");
const CustomException = require("../../../helpers/CustomException");

const update = async (id,data) => {

  const trx = await transaction.start(tiposLicencias.knex());

  try {
    const { nombre, descripcion, modulos } = data;

    const tipo_licencia = await tiposLicencias.query(trx).updateAndFetchById(id, {
      nombre, 
      descripcion
    });

    if(!tipo_licencia)
      throw new CustomException(400, "Id de tipo licencia invalido","Tipo de licencia inexistente.");

    //borro los que tenia
    await tiposLicencias.relatedQuery('modulos',trx)
      .for(tipo_licencia.id)
      .unrelate(); 

    if(modulos)
    { 
      //agrego los nuevos
      for(let item of modulos) 
      {
        await tiposLicenciasModulos.query(trx).insertAndFetch({
          id_tipo_licencia: tipo_licencia.id, 
          id_modulo: item.id,
          orden: item.orden
        });
      }
    }
    
    await trx.commit();
    return tipo_licencia;
  } 
  catch (ex) {
    await trx.rollback();
    throw ex;
  }
};

module.exports = update;