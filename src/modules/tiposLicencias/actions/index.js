const tiposLicencias = require("../../../models/TiposLicencias");
const filtros = require("../../../helpers/Filtros");

const index = async (req) => {
  try {

    const {limit,offset,order,sort} = filtros(req);

    const query = tiposLicencias.query();

    if (req.query.q && req.query.q != '') {
      query.orWhere('tipos_licencias.nombre', 'like', '%'+req.query.q+'%')
      query.orWhere('tipos_licencias.descripcion', 'like', '%'+req.query.q+'%')
    }

    const [cantidad, results] = await Promise.all([
      query.resultSize(),
      query.offset(offset).limit(limit).orderBy(order,sort)
                            .withGraphJoined('modulos')
                            //.withGraphJoined('tramites')
                            //.withGraphJoined('preguntas')



    ]);

    var resultset = {"count":cantidad,"offset":offset,"limit":limit};
    var metadata = {resultset};

    return {
      metadata,
      results
    }
  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = index;
