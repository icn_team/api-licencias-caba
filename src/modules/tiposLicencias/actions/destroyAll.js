const { transaction } = require("objection");
const tiposLicencias = require("../../../models/TiposLicencias");
const tiposLicenciasModulos = require("../../../models/TiposLicenciasModulos");
const tramitesTiposLicencias = require("../../../models/TramitesTiposLicencias");
const preguntasTiposLicencias = require("../../../models/PreguntasTiposLicencias");

const CustomException = require("../../../helpers/CustomException");

const destroyAll = async () => {
  //comentar para habilitarlo
  return false;

  const trx = await transaction.start(tiposLicencias.knex());

  
  try {

    await preguntasTiposLicencias.query(trx).delete();
    await tramitesTiposLicencias.query(trx).delete();
    await tiposLicenciasModulos.query(trx).delete();
    let numberOfDeletedRows = await tiposLicencias.query(trx).delete();

    await trx.commit();
    return numberOfDeletedRows;
  } 
  catch (ex) {
    await trx.rollback();
    throw ex;
  }
};

module.exports = destroyAll;