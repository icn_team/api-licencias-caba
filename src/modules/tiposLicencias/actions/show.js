const tiposLicencias = require("../../../models/TiposLicencias");
const CustomException = require("../../../helpers/CustomException");

const show = async (id) => {
  try {
    let tipo_licencia = await tiposLicencias.query()
                        .withGraphFetched('modulos')
                        //.withGraphFetched('tramites')
                        //.withGraphFetched('preguntas')
                        .findById(id);

    if(!tipo_licencia)
      throw new CustomException(400, "Id de tipo licencia invalido","Tipo de licencia inexistente.");

    return tipo_licencia;

  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = show;