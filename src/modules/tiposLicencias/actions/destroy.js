const { transaction } = require("objection");
const tiposLicencias = require("../../../models/TiposLicencias");
const CustomException = require("../../../helpers/CustomException");

const destroy = async (id) => {

  const trx = await transaction.start(tiposLicencias.knex());

  try {
    //borro los que tenia
    await tiposLicencias.relatedQuery('modulos',trx)
      .for(id)
      .unrelate(); 

    //borro los que tenia
    await tiposLicencias.relatedQuery('preguntas',trx)
      .for(id)
      .unrelate(); 

    //borro los que tenia
    await tiposLicencias.relatedQuery('tramites',trx)
      .for(id)
      .unrelate(); 

    let numberOfDeletedRows = await tiposLicencias.query(trx).deleteById(id);

    if(numberOfDeletedRows == 0)
      throw new CustomException(400, "Id de tipo licencia invalido","Tipo de licencia inexistente.");

    await trx.commit();
    return numberOfDeletedRows;
  } 
  catch (ex) {
    await trx.rollback();    
    throw ex;
  }
};

module.exports = destroy;