const { transaction } = require("objection");
const tiposLicencias = require("../../../models/TiposLicencias");
const tiposLicenciasModulos = require("../../../models/TiposLicenciasModulos");
const CustomException = require("../../../helpers/CustomException");

const store = async (data) => {

  const trx = await transaction.start(tiposLicencias.knex());

  try {
    const { nombre, descripcion, modulos } = data;

    const tipo_licencia = await tiposLicencias.query(trx).insertAndFetch({
      nombre, 
      descripcion
    });

    if(modulos)
    { 
      for(let item of modulos) 
      {
        await tiposLicenciasModulos.query(trx).insertAndFetch({
          id_tipo_licencia: tipo_licencia.id, 
          id_modulo: item.id,
          orden: item.orden
        });
      }
    }
    
    await trx.commit();
    return tipo_licencia;
  } 
  catch (ex) {
    await trx.rollback();
    throw ex;
  }
};

module.exports = store;