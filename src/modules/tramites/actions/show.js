const tramites = require("../../../models/Tramites");
const CustomException = require("../../../helpers/CustomException");

const show = async (id) => {
  try {
    let tramite = await tramites.query()
                                      .withGraphJoined('tipo_tramite')
                                      .withGraphJoined('usuario')
                                      .withGraphJoined('tipos_licencias')
                                      .withGraphJoined('estado_tramite')
                                      .findById(id);

    if(!tramite)
      throw new CustomException(400, "Id de tramite invalido","Tramite inexistente.");

    return tramite;

  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = show;