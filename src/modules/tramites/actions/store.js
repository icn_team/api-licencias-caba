const { transaction } = require("objection");
const tramites = require("../../../models/Tramites");
const tramitesTiposLicencias = require("../../../models/TramitesTiposLicencias");

const CustomException = require("../../../helpers/CustomException");

const Ajv = require('ajv');
const ajv = new Ajv();
const schema = require("../../../schemas/tramites/insertTramite");
const validateRequest = ajv.compile(schema);

const store = async (datos) => {

  const trx = await transaction.start(tramites.knex());

  try {
    const { id_tipo_tramite, id_user, tipos_licencias, id_estado_tramite, fecha_curso_sincrono, presente_curso_sincrono, calificacion_curso_sincrono } = datos;

    if (!validateRequest(datos))
      throw new CustomException(400, validateRequest.errors,"Algunos datos ingresados son inválidos.");

    const tramite = await tramites.query(trx).insertAndFetch({
      id_tipo_tramite, 
      id_user,
      id_estado_tramite,
      fecha_curso_sincrono,
      presente_curso_sincrono,
      calificacion_curso_sincrono
    });

    if(tipos_licencias)
    { 
      for(let item of tipos_licencias) 
      {
        await tramitesTiposLicencias.query(trx).insertAndFetch({
          id_tramite: tramite.id, 
          id_tipo_licencia: item
        });
      }
    }

    await trx.commit();
    return tramite;
  } 
  catch (ex) {
    await trx.rollback();    
    throw ex;
  }
};

module.exports = store;