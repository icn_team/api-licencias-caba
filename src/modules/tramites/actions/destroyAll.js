const { transaction } = require("objection");
const tramites = require("../../../models/Tramites");
const tramitesTiposLicencias = require("../../../models/TramitesTiposLicencias");
const CustomException = require("../../../helpers/CustomException");

const destroyAll = async () => {

  //comentar para habilitarlo
  return false;
  
  const trx = await transaction.start(tramites.knex());

  try {
    await tramitesTiposLicencias.query(trx).delete();

    let numberOfDeletedRows = await tramites.query(trx).delete();

    await trx.commit();
    return numberOfDeletedRows;
  } 
  catch (ex) {
    await trx.rollback();    
    throw ex;
  }
};

module.exports = destroyAll;