const tramites = require("../../../models/Tramites");
const filtros = require("../../../helpers/Filtros");

const index = async (req) => {
  try {

    const {limit,offset,order,sort} = filtros(req);

    const query = tramites.query();

    if (req.query.id_user && req.query.id_user != '') {
      query.where('id_user', req.query.id_user)
    }

    const [cantidad, results] = await Promise.all([
      query.resultSize(),
      query.offset(offset).limit(limit).orderBy(order,sort)
                            .withGraphJoined('tipo_tramite')
                            .withGraphJoined('usuario')
                            .withGraphJoined('tipos_licencias')
                            .withGraphJoined('estado_tramite')
    ]);

    var resultset = {"count":cantidad,"offset":offset,"limit":limit};
    var metadata = {resultset};

    return {
      metadata,
      results
    }
  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = index;
