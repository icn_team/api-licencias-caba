const { transaction } = require("objection");
const tramites = require("../../../models/Tramites");
const CustomException = require("../../../helpers/CustomException");

const destroy = async (id) => {

  const trx = await transaction.start(tramites.knex());

  try {

    //borro los que tenia
    await tramites.relatedQuery('tipos_licencias',trx)
      .for(id)
      .unrelate();

    let numberOfDeletedRows = await tramites.query(trx).deleteById(id);

    if(numberOfDeletedRows == 0)
      throw new CustomException(400, "Id de tramite invalido","Tramite inexistente.");

    await trx.commit();
    return numberOfDeletedRows;
  } 
  catch (ex) {
    await trx.rollback();
    throw ex;
  }
};

module.exports = destroy;