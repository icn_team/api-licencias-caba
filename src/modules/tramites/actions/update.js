const { transaction } = require("objection");
const tramites = require("../../../models/Tramites");
const tramitesTiposLicencias = require("../../../models/TramitesTiposLicencias");

const CustomException = require("../../../helpers/CustomException");

const Ajv = require('ajv');
const ajv = new Ajv();
const schema = require("../../../schemas/tramites/updateTramite");
const validateRequest = ajv.compile(schema);

const update = async (id,datos) => {

  const trx = await transaction.start(tramites.knex());

  try {
    const { id_tipo_tramite, id_user, tipos_licencias, id_estado_tramite, fecha_curso_sincrono, presente_curso_sincrono, calificacion_curso_sincrono } = datos;

    if (!validateRequest(datos))
      throw new CustomException(400, validateRequest.errors,"Algunos datos ingresados son inválidos.");

    const tramite = await tramites.query(trx).updateAndFetchById(id, {
      id_tipo_tramite, 
      id_user,
      id_estado_tramite,
      fecha_curso_sincrono,
      presente_curso_sincrono,
      calificacion_curso_sincrono
    });

    if(!tramite)
      throw new CustomException(400, "Id de tramite invalido","Tramite inexistente.");

    //borro los que tenia
    await tramites.relatedQuery('tipos_licencias',trx)
      .for(tramite.id)
      .unrelate(); 

    if(tipos_licencias)
    { 
      //agrego los nuevos
      for(let item of tipos_licencias) 
      {
        await tramitesTiposLicencias.query(trx).insertAndFetch({
          id_tramite: tramite.id, 
          id_tipo_licencia: item
        });
      }
    }

    await trx.commit();    
    return tramite;
  } 
  catch (ex) {
    await trx.rollback();     
    throw ex;
  }
};

module.exports = update;