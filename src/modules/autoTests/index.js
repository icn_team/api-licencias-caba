module.exports = {
  update: require("./actions/update"),
  store: require("./actions/store"),
  index: require("./actions/index"),
  show: require("./actions/show")    
};
  