const { transaction } = require("objection");
const autoTests = require("../../../models/AutoTests");
const CustomException = require("../../../helpers/CustomException");
const tiposLicencias = require("../../../models/TiposLicencias");
const tramites = require("../../../models/Tramites");
const autoTestsPreguntas = require("../../../models/AutoTestsPreguntas");

const store = async (datos) => {

  const trx = await transaction.start(autoTests.knex());

  try {
    const { id_tramite, id_tipo_licencia } = datos;

    let tramite = await tramites.query()
                                      .findById(id_tramite);

    if(!tramite)
      throw new CustomException(400, "Id de tramite invalido","Tramite inexistente.");



    //busco las preguntas para este tipo de licencia.
    var preguntas_x_tipo_licencia = await tiposLicencias.query()
                                              .findById(id_tipo_licencia)
                                              .withGraphJoined('preguntas');


    if(!preguntas_x_tipo_licencia)
      throw new CustomException(400, "Id de tipo de licencia es invalido","Tipo de Licencia inexistente.");

    var preguntas_salida = generoPreguntas(preguntas_x_tipo_licencia);

    if(preguntas_salida.length == 0)
      throw new CustomException(400, "No Existen preguntas para este tipo de licencias","No Existen preguntas para este tipo de licencias.");

    //creo auto test
    var auto_test = await autoTests.query(trx).insertAndFetch({
      id_tramite,
      id_tipo_licencia,
      acertividad: 0,
      id_estado: 1
    });

    //creo las preguntas en auto_test_ptrguntas
    for(let item of preguntas_salida) 
    {
        await autoTestsPreguntas.query(trx).insertAndFetch({
          id_auto_test : auto_test.id, 
          id_pregunta: item.id,
          data_copia: item
        });
    }

    await trx.commit();
    return auto_test;
  } 
  catch (ex) {
    await trx.rollback();
    throw ex;
  }
};

function preguntasRandomCant(lista, cant) {
  return [...lista].sort(() => (Math.random() > 0.5 ? 1 : -1)).slice(0, cant)
}

function generoPreguntas(preguntas_x_tipo_licencia){

  /*cantidad de preguntas segun tema a buscar dentro de las 40 preguntas random
  1 Generalidades (3)
  2 Prioridades de Paso (5)
  3 Velocidad (3)
  4 Capacidad Natural (4)
  5 Estacionamiento (3)
  6 Elementos de Seguridad (5)
  7 Situaciones Adversas (4)
  8 Legales y Documentación (3)
  9 Señales (5)
  10  De relevancia (5)
  */

  //creo arrays para guardar las preguntas por tema, si despues hago random dentro de cada array
  var generalidades = [];
  var prioridades_paso = [];
  var velocidad = [];
  var capacidad_natural = [];
  var estacionamiento = [];
  var elemento_seguridad = [];
  var situaciones_adversas = [];
  var legales_documentacion = [];
  var senales = [];
  var de_relevancia = [];

  var preguntas_salida = [];

  if(preguntas_x_tipo_licencia.preguntas.length > 0)
  {    
    for(let item of preguntas_x_tipo_licencia.preguntas) 
    {
      if(item.id_tema == 1)
      {
        generalidades.push(item);
      }

      if(item.id_tema == 2)
      {
        prioridades_paso.push(item);
      }

      if(item.id_tema == 3)
      {
        velocidad.push(item);
      }

      if(item.id_tema == 4)
      {
        capacidad_natural.push(item);
      }

      if(item.id_tema == 5)
      {
        estacionamiento.push(item);
      }

      if(item.id_tema == 6)
      {
        elemento_seguridad.push(item);
      }

      if(item.id_tema == 7)
      {
        situaciones_adversas.push(item);
      }

      if(item.id_tema == 8)
      {
        legales_documentacion.push(item);
      }

      if(item.id_tema == 9)
      {
        senales.push(item);
      }

      if(item.id_tema == 10)
      {
        de_relevancia.push(item);
      }
    }

    preguntas_salida = [
      ...preguntasRandomCant(generalidades,3),
      ...preguntasRandomCant(prioridades_paso,5),
      ...preguntasRandomCant(velocidad,3),
      ...preguntasRandomCant(capacidad_natural,4),
      ...preguntasRandomCant(estacionamiento,3),
      ...preguntasRandomCant(elemento_seguridad,5),
      ...preguntasRandomCant(situaciones_adversas,4),
      ...preguntasRandomCant(legales_documentacion,3),
      ...preguntasRandomCant(senales,5),
      ...preguntasRandomCant(de_relevancia,5)
    ];
  } 

  return preguntas_salida;
}

module.exports = store;