const autoTest = require("../../../models/AutoTests");
const filtros = require("../../../helpers/Filtros");

const index = async (req) => {
  try {

    const {limit,offset,order,sort} = filtros(req);

    const query = autoTest.query();

    //filtro por id_tramite
    if(req.query.id_tramite && req.query.id_tramite != '')
    {
      var arr_ids_tramites = req.query.id_tramite.split(",");
    
      query.whereIn('id_tramite',  arr_ids_tramites)
    }
    
    //filtro por id_tipo_licencia
    if(req.query.id_tipo_licencia && req.query.id_tipo_licencia != '')
    {
      var arr_ids_tipos_licencias = req.query.id_tipo_licencia.split(",");
    
      query.whereIn('id_tipo_licencia',  arr_ids_tipos_licencias)
    }


    const [cantidad, results] = await Promise.all([
      query.resultSize(),
      query.offset(offset).limit(limit).orderBy(order,sort)
                                         .withGraphJoined('tramite')
                                         .withGraphJoined('tipo_licencia')
                                         .withGraphJoined('estado')
                                         .withGraphFetched('auto_tests_preguntas')

                                         
    ]);

    var resultset = {"count":cantidad,"offset":offset,"limit":limit};
    var metadata = {resultset};

    return {
      metadata,
      results
    }
  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = index;
