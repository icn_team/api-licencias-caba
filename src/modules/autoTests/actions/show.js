const autoTests = require("../../../models/AutoTests");
const CustomException = require("../../../helpers/CustomException");

const show = async (id) => {
  try {
    let auto_test = await autoTests.query().findById(id).withGraphFetched('auto_tests_preguntas');

    if(!auto_test)
      throw new CustomException(400, "Id de auto test inexistente","Auto test inexistente.");

    return auto_test;

  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = show;