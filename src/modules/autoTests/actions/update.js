const autoTests = require("../../../models/AutoTests");
const CustomException = require("../../../helpers/CustomException");

const update = async (id,datos) => {
  try {
    const { acertividad, tiempo_ini, id_estado } = datos;

    const auto_tests = await autoTests.query().updateAndFetchById(id, {
      acertividad,
      tiempo_ini,
      id_estado
    });

    if(!auto_tests)
      throw new CustomException(400, "Id de autoTest invalido","autoTest inexistente.");

    return auto_tests;

  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = update;