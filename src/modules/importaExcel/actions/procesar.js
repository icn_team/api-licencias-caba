const { transaction } = require("objection");
const preguntasTiposLicencias = require("../../../models/PreguntasTiposLicencias");
const preguntas2 = require("../../../models/Preguntas");

const reader = require('xlsx');

const procesar = async (data) => {

  const trx = await transaction.start(preguntas2.knex());

  try {
    const file = reader.readFile('uploads/preguntas.xlsx')

    let data = []
    var cant = 0;
    var ultimo_id = 0;
    var parte = []
      
    const sheets = file.SheetNames
      
    for(let i = 0; i < sheets.length; i++)
    {
       const temp = reader.utils.sheet_to_json(
            file.Sheets[file.SheetNames[i]])
       temp.forEach((res) => {

          data.push(res)

       })
    }   

    var bloque = []
    var salida = []
    data.forEach(function(valor, indice, array)
    {
      if(valor.a)
      {
        if(bloque.length > 0)
        {          
          cant++;
          salida.push({id:cant,bloque})
          bloque = []
        }
        bloque.push(valor)
      }
      else
      {
        bloque.push(valor)
      }
    })


    var preguntas = []
    salida.forEach(function(valor, indice, array)
    {
      //console.log(valor.bloque)

      var preg = {}
      var respuesta = []
      var respuesta_id = 1;

      var n_pregunta = valor.bloque[0].b; 
      var categoria = valor.bloque[0].c;
      var pregunta = valor.bloque[0].d;
      respuesta.push({id:respuesta_id,texto:valor.bloque[0].e});

      if(valor.bloque[0].f)
        var correcta = respuesta_id

      var n_imagen = valor.bloque[0].g;
      var manual = valor.bloque[0].l;
      var tema = valor.bloque[0].m;
      var n_tema = valor.bloque[0].n;

      for(let i=1;i<valor.bloque.length;i++)
      {
        respuesta_id++

        respuesta.push({id:respuesta_id,texto:valor.bloque[i].e});

        if(valor.bloque[i].f)
          var correcta = respuesta_id
      }

      preg.n_pregunta = n_pregunta;
      preg.categoria = categoria;
      preg.pregunta = pregunta;
      preg.respuesta = respuesta;
      preg.correcta = correcta;
      preg.n_imagen = n_imagen;
      preg.manual = manual;
      preg.tema = tema;
      preg.n_tema = n_tema;

      preguntas.push(preg)


    })


    //borro tabla preguntas y relaciones
    await preguntasTiposLicencias.query(trx).delete();
    await preguntas2.query(trx).delete();

    //lleno tabla preguntas y relaciones
    n_pregunta_actual = 0;
    for(let item of preguntas) 
    {
      var tipo_lic = 9;
      if(item.categoria == "A") tipo_lic = 1;
      if(item.categoria == "B") tipo_lic = 2;
      if(item.categoria == "D") tipo_lic = 3;
      // if(item.categoria == "D") tipo_lic = 4;
      // if(item.categoria == "E") tipo_lic = 5;

      var id_tema = 13;
      if(item.n_tema) id_tema = item.n_tema;

      if(id_tema == 13 || tipo_lic == 9) continue;

      //lleno la pregunta
      if(item.n_pregunta != n_pregunta_actual)
      {
        var preg = await preguntas2.query(trx).insertAndFetch({
          id_tema: id_tema,
          data: {
            pregunta: item.pregunta,
            respuestas: item.respuesta,
            id_respuesta_correcta: item.correcta,
            id_imagen: item.n_imagen,
            manual: item.manual
          }
        });

        n_pregunta_actual = item.n_pregunta;
      }


      //lleno relacion
      await preguntasTiposLicencias.query(trx).insertAndFetch({
        id_pregunta: preg.id, 
        id_tipo_licencia: tipo_lic
      });

    }



    //console.log(preguntas[99])


    await trx.commit();
    return preguntas;
  } 
  catch (ex) {
    await trx.rollback();
    throw ex;
  }
};

module.exports = procesar;



// const parseExcel = (filename) => {

//     const excelData = XLSX.readFile(filename);

//     return Object.keys(excelData.Sheets).map(name => ({
//         name,
//         data: XLSX.utils.sheet_to_json(excelData.Sheets[name]),
//     }));
// };

// parseExcel("./ss.xls").forEach(element => {
//     console.log(element.data);
// });




    // const file = reader.readFile('uploads/preguntas.xlsx')

    // let data = []
      
    // const sheets = file.SheetNames
      
    // for(let i = 0; i < sheets.length; i++)
    // {
    //    const temp = reader.utils.sheet_to_json(
    //         file.Sheets[file.SheetNames[i]])
    //    temp.forEach((res) => {
    //       data.push(res)
    //    })
    // }
      
    // // Printing data
    // console.log(data)