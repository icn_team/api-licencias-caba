const { transaction } = require("objection");
const modulos = require("../../../models/Modulos");
const CustomException = require("../../../helpers/CustomException");

const destroy = async (id) => {

  const trx = await transaction.start(modulos.knex());

  try {

    //borro los que tenia
    await modulos.relatedQuery('contenidos',trx)
      .for(id)
      .unrelate(); 

    //borro los que tenia
    await modulos.relatedQuery('tipos_licencias',trx)
      .for(id)
      .unrelate(); 

    let numberOfDeletedRows = await modulos.query(trx).deleteById(id);

    if(numberOfDeletedRows == 0)
      throw new CustomException(400, "Id de modulo invalido","Modulo inexistente.");

    await trx.commit();
    return numberOfDeletedRows;
  } 
  catch (ex) {
    await trx.rollback();
    throw ex;
  }
};

module.exports = destroy;