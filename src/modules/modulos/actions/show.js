const modulos = require("../../../models/Modulos");
const CustomException = require("../../../helpers/CustomException");

const show = async (id) => {
  try {
    let modulo = await modulos.query().findById(id).withGraphJoined('contenidos');

    if(!modulo)
      throw new CustomException(400, "Id de modulo","Modulo inexistente.");

    return modulo;

  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = show;