const { transaction } = require("objection");
const modulos = require("../../../models/Modulos");
const modulosContenidos = require("../../../models/ModulosContenidos");
const CustomException = require("../../../helpers/CustomException");

const store = async (data) => {

  const trx = await transaction.start(modulos.knex());

  try {
    const { nombre, descripcion, contenidos } = data;

    const modulo = await modulos.query(trx).insertAndFetch({
      nombre, 
      descripcion
    });

    if(contenidos)
    {    
      for(let item of contenidos) 
      {
        await modulosContenidos.query(trx).insertAndFetch({
          id_modulo: modulo.id, 
          id_contenido: item.id,
          orden: item.orden
        });
      }
    }


    await trx.commit();
    return modulo;
  } 
  catch (ex) {
    await trx.rollback();
    throw ex;
  }
};

module.exports = store;