const { transaction } = require("objection");
const modulos = require("../../../models/Modulos");
const modulosContenidos = require("../../../models/ModulosContenidos");
const CustomException = require("../../../helpers/CustomException");

const update = async (id,data) => {

  const trx = await transaction.start(modulos.knex());

  try {
    const { nombre, descripcion, contenidos } = data;

    const modulo = await modulos.query(trx).updateAndFetchById(id, {
      nombre, 
      descripcion
    });

    if(!modulo)
      throw new CustomException(400, "Id de modulo invalido","Modulo inexistente.");

    //borro los que tenia
    await modulos.relatedQuery('contenidos',trx)
      .for(modulo.id)
      .unrelate(); 

    if(contenidos)
    { 
      //agrego los nuevos
      for(let item of contenidos) 
      {
        await modulosContenidos.query(trx).insertAndFetch({
          id_modulo: modulo.id, 
          id_contenido: item.id,
          orden: item.orden
        });
      }
    }

    await trx.commit();
    return modulo;
  } 
  catch (ex) {
    await trx.rollback();
    throw ex;
  }
};

module.exports = update;