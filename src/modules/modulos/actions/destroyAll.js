const { transaction } = require("objection");
const modulos = require("../../../models/Modulos");
const modulosContenidos = require("../../../models/ModulosContenidos");
const tiposLicenciasModulos = require("../../../models/TiposLicenciasModulos");
const CustomException = require("../../../helpers/CustomException");

const destroyAll = async () => {
  //comentar para habilitarlo
  return false;

  const trx = await transaction.start(modulos.knex());

  try {
    await modulosContenidos.query(trx).delete();
    await tiposLicenciasModulos.query(trx).delete();
    let numberOfDeletedRows = await modulos.query(trx).delete();

    await trx.commit();
    return numberOfDeletedRows;
  } 
  catch (ex) {
    await trx.rollback();
    throw ex;
  }
};

module.exports = destroyAll;