const autoTestsPreguntas = require("../../../models/AutoTestsPreguntas");
const CustomException = require("../../../helpers/CustomException");
const db = require("../../../../src/infra/database/index");

const update = async (id_auto_test,id_pregunta,datos) => {
  try {
    const { rta } = datos;

    let auto_tests_preguntas = await autoTestsPreguntas.query()
                                .where('id_auto_test',id_auto_test)
                                .where('id_pregunta',id_pregunta)
                                .first();
                    

    if(!auto_tests_preguntas)
      throw new CustomException(400, "Id de id_auto_test y/o id_pregunta invalido","autoTestsPreguntas inexistente.");

    await db.raw(`
                  UPDATE auto_tests_preguntas set rta = ${rta},updated_at = now()
                  WHERE id_auto_test = ${id_auto_test} AND id_pregunta = ${id_pregunta}
                `);


    let salida = await autoTestsPreguntas.query()
                                .where('id_auto_test',id_auto_test)
                                .where('id_pregunta',id_pregunta)
                                .first();

    return salida;

  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = update;