const tiposTramites = require("../../../models/TiposTramites");
const CustomException = require("../../../helpers/CustomException");

const store = async (data) => {

  try {
    const { nombre, descripcion } = data;

    const tipo_tramite = await tiposTramites.query().insertAndFetch({
      nombre, 
      descripcion
    });

    return tipo_tramite;
  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = store;