const tiposTramites = require("../../../models/TiposTramites");
const CustomException = require("../../../helpers/CustomException");

const destroy = async (id) => {

  try {
    let numberOfDeletedRows = await tiposTramites.query().deleteById(id);

    if(numberOfDeletedRows == 0)
      throw new CustomException(400, "Id de tipo tramite invalido","Tipo de tramite inexistente.");

    return numberOfDeletedRows;
  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = destroy;