const tiposTramites = require("../../../models/TiposTramites");
const CustomException = require("../../../helpers/CustomException");

const update = async (id,data) => {

  try {
    const { nombre, descripcion } = data;

    const tipo_tramite = await tiposTramites.query().updateAndFetchById(id, {
      nombre, 
      descripcion
    });

    if(!tipo_tramite)
      throw new CustomException(400, "Id de tipo tramite invalido","Tipo de tramite inexistente.");

    return tipo_tramite;
  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = update;