const tiposTramites = require("../../../models/TiposTramites");
const CustomException = require("../../../helpers/CustomException");

const show = async (id) => {
  try {
    let tipo_tramite = await tiposTramites.query().findById(id);

    if(!tipo_tramite)
      throw new CustomException(400, "Id de tipo tramite invalido","Tipo de tramite inexistente.");

    return tipo_tramite;

  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = show;