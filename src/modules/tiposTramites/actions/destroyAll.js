const tiposTramites = require("../../../models/TiposTramites");
const CustomException = require("../../../helpers/CustomException");

const destroyAll = async () => {

  //comentar para habilitarlo
  return false;
  
  try {
    let numberOfDeletedRows = await tiposTramites.query().delete();

    return numberOfDeletedRows;
  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = destroyAll;