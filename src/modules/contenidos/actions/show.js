const contenidos = require("../../../models/Contenidos");
const CustomException = require("../../../helpers/CustomException");

const show = async (id) => {
  try {
    let contenido = await contenidos.query().findById(id).withGraphJoined('tipo_contenido');

    if(!contenido)
      throw new CustomException(400, "Id de contenido","Contenido inexistente.");

    return contenido;

  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = show;