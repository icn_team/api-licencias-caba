const tramitesContenidosAvance = require("../../../models/TramitesContenidosAvance");
const CustomException = require("../../../helpers/CustomException");

const getAvance = async (id_tramite,id_contenido) => {

  try {

    let tramites_contenidos_avance = await tramitesContenidosAvance.query()
                                .where('id_tramite',id_tramite)
                                .where('id_contenido',id_contenido)
                                .first();

    if(!tramites_contenidos_avance)
      throw new CustomException(400, "Id de tramites contenidos invalido","tramites contenidos inexistente.");

    return tramites_contenidos_avance;
  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = getAvance;