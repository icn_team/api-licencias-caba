const { transaction } = require("objection");
const contenidos = require("../../../models/Contenidos");
const CustomException = require("../../../helpers/CustomException");

const destroy = async (id) => {

  const trx = await transaction.start(contenidos.knex());

  try {
    //borro los que tenia
    await contenidos.relatedQuery('modulos',trx)
      .for(id)
      .unrelate(); 

    let numberOfDeletedRows = await contenidos.query(trx).deleteById(id);

    if(numberOfDeletedRows == 0)
      throw new CustomException(400, "Id de contenido invalido","Contenido inexistente.");

    await trx.commit();
    return numberOfDeletedRows;
  } 
  catch (ex) {
    await trx.rollback();
    throw ex;
  }
};

module.exports = destroy;