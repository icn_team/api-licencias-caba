const { transaction } = require("objection");
const contenidos = require("../../../models/Contenidos");
const modulosContenidos = require("../../../models/ModulosContenidos");
const CustomException = require("../../../helpers/CustomException");

const destroyAll = async () => {

  //comentar para habilitarlo
  return false;
  
  const trx = await transaction.start(contenidos.knex());

  try {
    await modulosContenidos.query(trx).delete();
    let numberOfDeletedRows = await contenidos.query(trx).delete();

    await trx.commit();
    return numberOfDeletedRows;
  } 
  catch (ex) {
    await trx.rollback();
    throw ex;
  }
};

module.exports = destroyAll;