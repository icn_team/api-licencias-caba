const contenidos = require("../../../models/Contenidos");
const CustomException = require("../../../helpers/CustomException");

const update = async (id,datos) => {

  try {
    const { nombre, data, id_tipo_contenido } = datos;

    const contenido = await contenidos.query().updateAndFetchById(id, {
      nombre, 
      data,
      id_tipo_contenido
    });

    if(!contenido)
      throw new CustomException(400, "Id de contenido invalido","Contenido inexistente.");

    return contenido;
  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = update;