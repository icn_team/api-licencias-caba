const CustomException = require("../../../helpers/CustomException");
const db = require("../../../infra/database/index");

const getSecuancia = async (id_tramite) => {

  try {
    response = await db.raw(`
      SELECT 
        t.id AS id_tramite,
        ttl.id_tipo_licencia AS id_tipo_licencia,
        tlm.id_modulo,
        m.nombre AS nombre_modulo,
        tlm.orden AS orden_modulo,
        c.id AS id_contenido,
        c.nombre AS nombre_contenido,
        mc.orden AS orden_contenico,
        tca.completado,
        tca.avance
      FROM tramites AS t
      INNER JOIN 	tramites_tipos_licencias AS ttl ON ttl.id_tramite = t.id
      INNER JOIN 	tipos_licencias_modulos AS tlm ON tlm.id_tipo_licencia = ttl.id_tipo_licencia
      INNER JOIN 	modulos AS m ON m.id = tlm.id_modulo
      LEFT JOIN 	modulos_contenidos AS mc ON mc.id_modulo = tlm.id_modulo
      LEFT JOIN 	contenidos AS c ON mc.id_contenido = c.id
      LEFT JOIN 	tramites_contenidos_avance AS tca ON tca.id_contenido = c.id AND  tca.id_tramite = t.id
      WHERE t.id = ${id_tramite}
      GROUP BY t.id, tlm.id_modulo,  c.id
      ORDER BY tlm.orden, mc.orden
    `);

    if(!id_tramite)
      throw new CustomException(400, "Id de tramite inexistente.");

    return response[0];
  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = getSecuancia;