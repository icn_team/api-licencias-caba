const contenidos = require("../../../models/Contenidos");
const CustomException = require("../../../helpers/CustomException");

// const Ajv = require('ajv');
// const ajv = new Ajv();

//Guia de lectura con video
// const schema_1 = {
//   type: "object",
//   properties: {
//     foo: {type: "string"},
//     bar: {type: "number", maximum: 3},
//   },
//   required: ["foo", "bar"],
//   additionalProperties: false,
// }

//Contenido interactivo
// const schema_2 = {
//   type: "object",
//   properties: {
//     foo: {type: "string"},
//     bar: {type: "number", maximum: 3},
//   },
//   required: ["foo", "bar"],
//   additionalProperties: false,
// }

//Actividad
// const schema_3 = {
//   type: "object",
//   properties: {
//     foo: {type: "string"},
//     bar: {type: "number", maximum: 3},
//   },
//   required: ["foo", "bar"],
//   additionalProperties: false,
// }

// const validateRequestTipo1 = ajv.compile(schema_1);
// const validateRequestTipo2 = ajv.compile(schema_2);
// const validateRequestTipo3 = ajv.compile(schema_3);

/*
{
  "type": "object",
  "required": ["nombre", "email", "reclamo"],
  "properties": {
      "nombre": {"type": "string"},
      "email": {"type": "string", "format": "email"},
      "telefono": {"type": "string"},
      "reclamo": {"type": "string", "minLenght": 5}
  }
}
*/



const store = async (datos) => {

  try {
    const { nombre, data, id_tipo_contenido } = datos;

    // if(id_tipo_contenido == 1)
    // {
    //   if (!validateRequestTipo1(datos)) {
    //     return {
    //       status: 400,
    //       data: {
    //         code: 400,
    //         message: "Algunos datos ingresados son inválidos.",
    //         errors: validateRequestTipo1.errors
    //       }
    //     };
    //   }      
    // }
    // if(id_tipo_contenido == 2)
    // {
    //   if (!validateRequestTipo2(datos)) {
    //     return {
    //       status: 400,
    //       data: {
    //         code: 400,
    //         message: "Algunos datos ingresados son inválidos.",
    //         errors: validateRequestTipo2.errors
    //       }
    //     };
    //   }      
    // }
    // if(id_tipo_contenido == 3)
    // {
    //   if (!validateRequestTipo3(datos)) {
    //     return {
    //       status: 400,
    //       data: {
    //         code: 400,
    //         message: "Algunos datos ingresados son inválidos.",
    //         errors: validateRequestTipo3.errors
    //       }
    //     };
    //   }      
    // }


    const contenido = await contenidos.query().insertAndFetch({
      nombre, 
      data,
      id_tipo_contenido
    });

    return contenido;
  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = store;