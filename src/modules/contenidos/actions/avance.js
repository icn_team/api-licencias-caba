const tramitesContenidosAvance = require("../../../models/TramitesContenidosAvance");
const CustomException = require("../../../helpers/CustomException");

const avance = async (datos,id_tramite,id_contenido) => {
  try {
    //si ya exsiste el par (id_tramite, id_contenido) hago update, sino insert
    let tramites_contenidos_avance = await tramitesContenidosAvance.query()
      .where('id_tramite',id_tramite)
      .where('id_contenido',id_contenido)
      .first();

    if(tramites_contenidos_avance) //ya existia, lo actualizo
    {
      let data = {};
      if(datos.avance!=undefined) data.avance = datos.avance;
      if(datos.completado!=undefined) data.completado = datos.completado;
      if(datos.respuesta!=undefined) data.respuesta = datos.respuesta;
      var tramites_contenidosNew = await tramites_contenidos_avance.$query().updateAndFetch(data)
      .where('id_tramite',id_tramite)
      .where('id_contenido',id_contenido);
    }
    else //no existia, lo inserto
    {
      let avance = (datos.avance!=undefined) ? datos.avance : 0;
      let completado = (datos.completado!=undefined) ? datos.completado : 0;
      let respuesta = (datos.respuesta!=undefined) ? datos.respuesta : null;
      var tramites_contenidosNew = await tramitesContenidosAvance.query().insertAndFetch({
        id_tramite, 
        id_contenido,
        avance,
        completado,
        respuesta
      });
    }

    return tramites_contenidosNew;
  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = avance;