module.exports = {
  index: require("./actions/index"),
  show: require("./actions/show"),
  store: require("./actions/store"),
  update: require("./actions/update"),
  destroy: require("./actions/destroy"),
  destroyAll: require("./actions/destroyAll"),
  avance: require("./actions/avance"), //put
  getAvance: require("./actions/getAvance"), 
  getSecuencia: require("./actions/getSecuencia"), 
};
  