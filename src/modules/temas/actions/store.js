const temas = require("../../../models/Temas");
const CustomException = require("../../../helpers/CustomException");

const store = async (data) => {

  try {
    const { nombre } = data;

    const tema = await temas.query().insertAndFetch({
      nombre
    });

    return tema;
  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = store;