const temas = require("../../../models/Temas");
const CustomException = require("../../../helpers/CustomException");

const update = async (id,data) => {

  try {
    const { nombre } = data;

    const tema = await temas.query().updateAndFetchById(id, {
      nombre
    });

    if(!tema)
      throw new CustomException(400, "Id de tema invalido","Tema inexistente.");

    return tema;
  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = update;