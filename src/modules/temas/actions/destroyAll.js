const temas = require("../../../models/Temas");
const CustomException = require("../../../helpers/CustomException");

const destroyAll = async () => {

  //comentar para habilitarlo
  return false;
  
  try {
    let numberOfDeletedRows = await temas.query().delete();

    return numberOfDeletedRows;
  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = destroyAll;