const temas = require("../../../models/Temas");
const CustomException = require("../../../helpers/CustomException");

const show = async (id) => {
  try {
    let tema = await temas.query().findById(id);

    if(!tema)
      throw new CustomException(400, "Id de tema es invalido","Tema inexistente.");

    return tema;

  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = show;