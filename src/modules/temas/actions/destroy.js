const temas = require("../../../models/Temas");
const CustomException = require("../../../helpers/CustomException");

const destroy = async (id) => {

  try {
    let numberOfDeletedRows = await temas.query().deleteById(id);

    if(numberOfDeletedRows == 0)
      throw new CustomException(400, "Id de tema","Tema inexistente.");

    return numberOfDeletedRows;
  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = destroy;