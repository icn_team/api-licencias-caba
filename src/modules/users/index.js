module.exports = {
    create: require("./actions/create"),
    getByEmail: require("./actions/getByEmail"),
    login: require("./actions/login"),
  };
  