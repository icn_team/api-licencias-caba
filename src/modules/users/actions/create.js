const { transaction } = require("objection");
const Users = require("../../../models/users");
const Roles = require("../../../models/roles");
const bcrypt = require('bcrypt');
const validoUserDuplicado = require("./validoUserDuplicado");

const create = async (data) => {

  const trx = await transaction.start(Users.knex());

  try {
    const { nombre, apellido, dni, email, password } = data;
    const encriptedPass = await bcrypt.hash(password, 10);

    await validoUserDuplicado(data, trx);

    const user = await Users.query(trx).insertAndFetch({
      nombre, 
      apellido, 
      dni, 
      email, 
      password: encriptedPass
    });

    const rol = await Roles.query(trx).findById(2);
    await user.$relatedQuery('roles',trx).relate(rol);

    await trx.commit();

    return user;
  } 
  catch (ex) {
    await trx.rollback();
    throw ex;
  }
};

module.exports = create;