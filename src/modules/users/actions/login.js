require("dotenv").config();
const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken");
const { transaction } = require("objection");
const Users = require("../../../models/users");
const UserException = require("../../../helpers/UserException");


const login = async (body) => {
  try {
    if (!body.email) { throw new UserException(400, "Ingrese su dirección de correo electrónico.");}
    if (!body.password) { throw new UserException(400, "Ingrese su contraseña.");}

    const { email, password } = body;
    let user = await Users.query().where('email', email).withGraphFetched('roles').first();
    if (!user) { throw new UserException(400, "La dirección de correo electrónico no pertenece a un usuario registrado.");}

    //si la contraseña no es correcta
    if(!await bcrypt.compare(password, user.password)){
      throw new UserException(400, "Contraseña invalida");
    }

    //borra datos privados del usuario
    delete user.dni;
    delete user.password;
    delete user.updatedAt;

    
     //genera el token
    const token = jwt.sign(
      {
        sub: user.id,
        token_type: "bearer",
        user: user,
      },
      process.env.API_SECRET_KEY,
      {
        expiresIn: '8h',
      }
    );
    
    return {token:token};
  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = login;