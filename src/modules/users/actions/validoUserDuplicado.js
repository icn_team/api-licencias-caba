const Users = require("../../../models/users");
const CustomException = require("../../../helpers/CustomException");

const validoUserDuplicado = async (user) => {

  //valido dni duplicados
  const user_dni = await Users.query().where('dni', user.dni).first();
  if(user_dni)
    throw new CustomException(400, "DNI Duplicado.");

  //valido email duplicados
  const user_email = await Users.query().where('email', user.email).first();
  if(user_email)
    throw new CustomException(400, "Email Duplicado.");

};

module.exports = validoUserDuplicado;
