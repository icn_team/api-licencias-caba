const { transaction } = require("objection");
const Users = require("../../../models/users");


const getByEmail = async (email) => {
  try {
    let user = await Users.query().where('email', email).first();
    return user;
  } 
  catch (ex) {
    throw ex;
  }
};

module.exports = getByEmail;