"use strict";

const fs = require("fs");
const scorm = require("../../../../modules/scorm");

async function index(req, res, next){
  try {
    const results = await scorm.index(req);
    return res.status(200).json(results)

  }
  catch (ex) {
    next(ex);
  }
}

async function show(req, res, next){
  try {
    const result = await scorm.show(req.params.id);
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}

async function upload(req, res, next){
  try {
    const result = await scorm.upload(req);
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}

async function update(req, res, next){
  try {
    const result = await scorm.update(req.params.id,req);
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}

async function destroy(req, res, next){
  try {
    const result = await scorm.destroy(req.params.id);
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}





module.exports = {
  index,
  show,
  upload,
  update,
  destroy,
};
