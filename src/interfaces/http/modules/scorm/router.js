"use strict";

const express = require("express");
const api = express.Router();
const md_auth = require("../../../../middlewares/authenticated");
//const apiMiddleware = require("../../../../middlewares/api");
const partialResponse = require("express-partial-response");
const scormController = require("./index");
const upload = require("../../../../middlewares/upload");
const update = require("../../../../middlewares/update");



/** API-DOC
 * @swagger
 * /api/v1.0.0/scorms:
 *   get:
 *     tags:
 *     - Scorms
 *     summary: Retorna una lista de scorms
 *     description: Retorna una lista de scorms
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: fields 
 *         in: query
 *         description: PartialResponce - Ej. results(id)
 *         required: false
 *         type: string
 *       - name: limit
 *         in: query
 *         description: cantidad a obtener  - default 30 (-1 no limita)
 *         required: false
 *         type: integer
 *       - name: offset
 *         in: query
 *         description: cantidad a saltear - default 0
 *         required: false
 *         type: integer
 *       - name: order
 *         in: query
 *         description: campo por el cual ordenar - default id
 *         required: false
 *         type: string
 *       - name: sort
 *         in: query
 *         description: tipo de ordenamiento (asc o desc) - default asc
 *         required: false
 *         type: string
 *       - name: q
 *         in: query
 *         description: filtro aplicado a nombre o descripcion (like %q%) - Ej. amite
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Consulta exitosa
 */
api.get("/scorms",
  [ md_auth.ensureAuth, partialResponse()],
  scormController.index
);


 /**
 * @swagger
 * /api/v1.0.0/scorms/{id}:
 *   get:
 *     tags:
 *     - Scorms   
 *     summary: Retorna un scorm.
 *     description: Retorna un scorm.
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id del scorm a obtener.
 *         type: integer
 *       - name: fields 
 *         in: query
 *         description: PartialResponce - Ej. id
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Consulta exitosa
*/
api.get("/scorms/:id",
  [ md_auth.ensureAuth ,partialResponse()],
  scormController.show
);


/** API-DOC
 * @swagger
 * /api/v1.0.0/scorms:
 *  post:
 *    tags:
 *    - Scorms  
 *    summary: Registra un nuevo scorm
 *    description: Registra un nuevo scorm
 *    consumes:
 *      - multipart/form-data
 *    parameters:
 *      - name: file
 *        in: formData
 *        type: file
 *        description: archivo scorm.
 *        x-mimetype: application/zip
 *      - name: nombre
 *        in: formData
 *        description: nombre del scorm
 *      - name: descripcion
 *        in: formData
 *        description: descripcion del scorm
 *    responses:
 *      '200':
 *         description: Consulta exitosa
 */
api.post("/scorms",
  [ md_auth.ensureAuth,  upload.single("file") ],
   scormController.upload
);


 /**
 * @swagger
 * /api/v1.0.0/scorms/{id}:
 *   put:
 *     tags:
 *     - Scorms   
 *     summary: Actualiza un scorm.
 *     description: Actualiza un scorm.
 *     consumes:
 *       - multipart/form-data
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id del scorm.
 *         type: integer
 *       - name: file
 *         in: formData
 *         type: file
 *         description: archivo scorm.
 *         x-mimetype: application/zip
 *       - name: nombre
 *         in: formData
 *         description: nombre
 *       - name: descripcion
 *         in: formData
 *         description: descripcion
 *     responses:
 *       200:
 *         description: Consulta exitosa
*/

api.put("/scorms/:id",
  [ md_auth.ensureAuth ,update.single("file"),partialResponse()],
  scormController.update
);

 /**
 * @swagger
 * /api/v1.0.0/scorms/{id}:
 *   delete:
 *     tags:
 *     - Scorms   
 *     summary: Elimina un scorm.
 *     description: Elimina un scorm.
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id del scorm a eliminar.
 *         type: integer
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: integer
 *           description: cantidad de scorms borrados.
 *           example: 1
*/
api.delete("/scorms/:id",
  [ md_auth.ensureAuth ,partialResponse()],
  scormController.destroy
);

module.exports = api;

