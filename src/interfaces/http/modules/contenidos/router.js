"use strict";

const express = require("express");
const api = express.Router();
const md_auth = require("../../../../middlewares/authenticated");
//const apiMiddleware = require("../../../../middlewares/api");
const partialResponse = require("express-partial-response");
const contenidosController = require("./index");


/** API-DOC
 * @swagger
 * /api/v1.0.0/contenidos:
 *   get:
 *     tags:
 *     - Contenidos 
 *     summary: Retorna una lista de contenidos
 *     description: Retorna una lista de contenidos
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: fields 
 *         in: query
 *         description: PartialResponce - Ej. results(id)
 *         required: false
 *         type: string
 *       - name: limit
 *         in: query
 *         description: cantidad a obtener  - default 30 (-1 no limita)
 *         required: false
 *         type: integer
 *       - name: offset
 *         in: query
 *         description: cantidad a saltear - default 0
 *         required: false
 *         type: integer
 *       - name: order
 *         in: query
 *         description: campo por el cual ordenar - default id
 *         required: false
 *         type: string
 *       - name: sort
 *         in: query
 *         description: tipo de ordenamiento (asc o desc) - default asc
 *         required: false
 *         type: string
 *       - name: q
 *         in: query
 *         description: filtro aplicado a nombre (like %q%) - Ej. amite
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: object
 *           properties:
 *             metadata:
 *               type: object
 *               properties:
 *                 resultset:
 *                   type: object
 *                   properties:
 *                     count:
 *                       type: integer
 *                       example: 1
 *                     offset:
 *                       type: integer
 *                       example: 0 
 *                     limit:
 *                       type: integer
 *                       example: 30
 *             results:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   id:
 *                     type: integer
 *                     description: id de contenido.
 *                     example: 1
 *                   nombre:
 *                     type: string
 *                     description: El nombre del contenido.
 *                     example: contenido 1
 *                   data:
 *                     type: object
 *                     description: contenido.
 *                     example:
 *                   id_tipo_contenido:
 *                     type: integer
 *                     description: id de tipo contenido.
 *                     example: 1
 */
api.get("/contenidos",
  [ md_auth.ensureAuth, partialResponse()],
  contenidosController.index
);


 /**
 * @swagger
 * /api/v1.0.0/contenidos/{id}:
 *   get:
 *     tags:
 *     - Contenidos   
 *     summary: Retorna un contenido.
 *     description: Retorna un contenido.
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id del contenido a obtener.
 *         type: integer
 *       - name: fields 
 *         in: query
 *         description: PartialResponce - Ej. id
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: id de contenido.
 *               example: 1
 *             nombre:
 *               type: string
 *               description: El nombre del contenido.
 *               example: contenido 1
 *             data:
 *               type: object
 *               description: contenido.
 *               example:
 *             id_tipo_contenido:
 *               type: integer
 *               description: id de tipo contenido.
 *               example: 1
*/
api.get("/contenidos/:id",
  [ md_auth.ensureAuth ,partialResponse()],
  contenidosController.show
);


/** API-DOC
 * @swagger
 * /api/v1.0.0/contenidos:
 *  post:
 *    tags:
 *    - Contenidos  
 *    summary: Registra un nuevo contenido
 *    description: Registra un nuevo contenido
 *    parameters:
 *      - name: body
 *        in: body
 *        description: Datos del contenido
 *        required:
 *          - nombre
 *          - data
 *          - id_tipo_contenido
 *        schema:
 *          type: object
 *          properties:
 *            nombre:
 *              type: string
 *            data:
 *              type: object
 *              example: {}
 *            id_tipo_contenido:
 *              type: integer
 *              example: 1
 *    responses:
 *      '200':
 *         description: Consulta exitosa
 *         schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: id de contenido.
 *               example: 1
 *             nombre:
 *               type: string
 *               description: El nombre del contenido.
 *               example: contenido 1
 *             data:
 *               type: object
 *               description: contenido.
 *               example: {}
 *             id_tipo_contenido:
 *               type: integer
 *               description: id de tipo contenido.
 *               example: 1
 */
api.post("/contenidos",
  [ md_auth.ensureAuth, partialResponse()],
  contenidosController.store
);


 /**
 * @swagger
 * /api/v1.0.0/contenidos/{id}:
 *   put:
 *     tags:
 *     - Contenidos   
 *     summary: Actualiza un contenido.
 *     description: Actualiza un contenido.
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id del contenido a actualizar.
 *         type: integer
 *       - name: body
 *         in: body
 *         description: Datos del contenido
 *         required:
 *           - nombre
 *           - data
 *           - id_tipo_contenido
 *         schema:
 *           type: object
 *           properties:
 *             nombre:
 *               type: string
 *             data:
 *               type: object
 *             id_tipo_contenido:
 *               type: integer
 *               example: 1
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: id de contenido.
 *               example: 1
 *             nombre:
 *               type: string
 *               description: El nombre del contenido.
 *               example: contenido 1
 *             data:
 *               type: object
 *               description: contenido.
 *               example:
 *             id_tipo_contenido:
 *               type: integer
 *               description: id de tipo contenido.
 *               example: 1
*/
api.put("/contenidos/:id",
  [ md_auth.ensureAuth ,partialResponse()],
  contenidosController.update
);

 /**
 * @swagger
 * /api/v1.0.0/contenidos/{id}:
 *   delete:
 *     tags:
 *     - Contenidos   
 *     summary: Elimina un contenido.
 *     description: Elimina un contenido.
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id del contenido a eliminar.
 *         type: integer
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: integer
 *           description: cantidad de contenidos borrados.
 *           example: 1
*/
api.delete("/contenidos/:id",
  [ md_auth.ensureAuth ,partialResponse()],
  contenidosController.destroy
);

 /**
 * @swagger
 * /api/v1.0.0/contenidos:
 *   delete:
 *     tags:
 *     - Contenidos   
 *     summary: Elimina todos los contenidos.
 *     description: Elimina todos los contenidos.
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: integer
 *           description: cantidad de contenidos borrados.
 *           example: 12
*/
api.delete("/contenidos",
  [ md_auth.ensureAuth ,partialResponse()],
  contenidosController.destroyAll
);

 /**
 * @swagger
 * /api/v1.0.0/contenidos/{id_contenido}/tramites/{id_tramite}/avances:
 *   put:
 *     tags:
 *     - Contenidos   
 *     summary: Crea o actualiza el avance del contenido para el tramite.
 *     description: Crea o actualiza el avance del contenido para el tramite.
 *     parameters:
 *       - in: path
 *         name: id_tramite
 *         required: true
 *         description: El id del tramite.
 *         type: integer
 *       - in: path
 *         name: id_contenido
 *         required: true
 *         description: El id del contenido.
 *         type: integer
 *       - name: body
 *         in: body
 *         description: Datos del contenido
 *         schema:
 *           type: object
 *           properties:
 *             avance:
 *               type: integer
 *               example: 243
 *             completado:
 *               type: integer
 *               example: 0
 *             respuesta:
 *               type: object
 *     responses:
 *       200:
 *         description: Consulta exitosa
*/
api.put("/contenidos/:id_contenido/tramites/:id_tramite/avances",
  [ md_auth.ensureAuth ,partialResponse()],
  contenidosController.avance
);

 /**
 * @swagger
 * /api/v1.0.0/contenidos/{id_contenido}/tramites/{id_tramite}/avances:
 *   get:
 *     tags:
 *     - Contenidos   
 *     summary: obtiene el avance del contenido para el tramite.
 *     description: obtiene el avance del contenido para el tramite.
 *     parameters:
 *       - in: path
 *         name: id_tramite
 *         required: true
 *         description: El id del tramite.
 *         type: integer
 *       - in: path
 *         name: id_contenido
 *         required: true
 *         description: El id del contenido.
 *         type: integer
 *     responses:
 *       200:
 *         description: Consulta exitosa
*/
api.get("/contenidos/:id_contenido/tramites/:id_tramite/avances",
  [ md_auth.ensureAuth ,partialResponse()],
  contenidosController.getAvance
);

/**
 * @swagger
 * /api/v1.0.0/contenidos-tramite-secuencia/{id_tramite}:
 *   get:
 *     tags:
 *     - Contenidos   
 *     summary: obtiene el avance del contenido para el tramite.
 *     description: obtiene el avance del contenido para el tramite.
 *     parameters:
 *       - in: path
 *         name: id_tramite
 *         required: true
 *         description: El id del tramite.
 *         type: integer
 *     responses:
 *       200:
 *         description: Consulta exitosa
*/
api.get("/contenidos-tramite-secuencia/:id_tramite",
  [ md_auth.ensureAuth ,partialResponse()],
  contenidosController.secuencia
);

module.exports = api;

