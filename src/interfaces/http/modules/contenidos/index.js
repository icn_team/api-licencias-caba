"use strict";
const contenidos = require("../../../../modules/contenidos");

async function index(req, res, next){
  try {
    const results = await contenidos.index(req);
    return res.status(200).json(results)

  }
  catch (ex) {
    next(ex);
  }
}

async function show(req, res, next){
  try {
    const result = await contenidos.show(req.params.id);
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}

async function store(req, res, next){
  try {
    const result = await contenidos.store(req.body);
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}

async function update(req, res, next){
  try {
    const result = await contenidos.update(req.params.id,req.body);
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}

async function destroy(req, res, next){
  try {
    const result = await contenidos.destroy(req.params.id);
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}

async function destroyAll(req, res, next){
  try {
    const result = await contenidos.destroyAll();
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}

async function avance(req, res, next){
  try {
    const result = await contenidos.avance(req.body,req.params.id_tramite,req.params.id_contenido);
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}

async function getAvance(req, res, next){
  try {
    const result = await contenidos.getAvance(req.params.id_tramite,req.params.id_contenido);
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}

async function secuencia(req, res, next){
  try {
    const result = await contenidos.getSecuencia(req.params.id_tramite);
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}


module.exports = {
  index,
  show,
  store,
  update,
  destroy,
  destroyAll,
  avance,
  getAvance,
  secuencia
};
