"use strict";

const express = require("express");
const api = express.Router();
const md_auth = require("../../../../middlewares/authenticated");
//const apiMiddleware = require("../../../../middlewares/api");
const partialResponse = require("express-partial-response");
const preguntasController = require("./index");




 /**
 * @swagger
 * /api/v1.0.0/autotests/{id}/preguntas:
 *   get:
 *     tags:
 *     - Preguntas   
 *     summary: Retorna preguntas del auto test.
 *     description: Retorna preguntas del auto test.
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id del auto test.
 *         type: integer
 *       - name: fields 
 *         in: query
 *         description: PartialResponce - Ej. id
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Consulta exitosa
*/
// api.get("/autotests/:id/preguntas",
//   [ md_auth.ensureAuth ,partialResponse()],
//   preguntasController.show
// );

// module.exports = api;

