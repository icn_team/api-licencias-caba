"use strict";
const autoTests = require("../../../../modules/autoTests");


async function index(req, res, next){
  try {
    const result = await autoTests.index(req);
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}

async function store(req, res, next){
  try {
    const result = await autoTests.store(req.body);
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}

async function update(req, res, next){
  try {
    const result = await autoTests.update(req.params.id,req.body);
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}

async function show(req, res, next){
  try {
    const result = await autoTests.show(req.params.id);
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}

module.exports = {
  update,
  store,
  index,
  show
};
