"use strict";

const express = require("express");
const api = express.Router();
const md_auth = require("../../../../middlewares/authenticated");
//const apiMiddleware = require("../../../../middlewares/api");
const partialResponse = require("express-partial-response");
const autoTestController = require("./index");


/** API-DOC
 * @swagger
 * /api/v1.0.0/autotests:
 *   get:
 *     tags:
 *     - Auto Test 
 *     summary: Retorna una lista de autotests
 *     description: Retorna una lista de autotests
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: fields 
 *         in: query
 *         description: PartialResponce - Ej. results(id)
 *         required: false
 *         type: string
 *       - name: limit
 *         in: query
 *         description: cantidad a obtener  - default 30 (-1 no limita)
 *         required: false
 *         type: integer
 *       - name: offset
 *         in: query
 *         description: cantidad a saltear - default 0
 *         required: false
 *         type: integer
 *       - name: order
 *         in: query
 *         description: campo por el cual ordenar - default id
 *         required: false
 *         type: string
 *       - name: sort
 *         in: query
 *         description: tipo de ordenamiento (asc o desc) - default asc
 *         required: false
 *         type: string
 *       - name: id_tramite
 *         in: query
 *         description:  EJ - id_tramite=10,20
 *         required: false
 *         type: string
 *       - name: id_tipo_licencia
 *         in: query
 *         description: EJ - id_tipo_licencia=10,20
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Consulta exitosa
 */
api.get("/autotests",
  [ md_auth.ensureAuth, partialResponse()],
  autoTestController.index
);

 /**
 * @swagger
 * /api/v1.0.0/autotests/{id}:
 *   get:
 *     tags:
 *     - Auto Test   
 *     summary: Retorna un auto test.
 *     description: Retorna un auto test.
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id del auto test a obtener.
 *         type: integer
 *       - name: fields 
 *         in: query
 *         description: PartialResponce - Ej. id
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Consulta exitosa
*/
api.get("/autotests/:id",
  [ md_auth.ensureAuth ,partialResponse()],
  autoTestController.show
);


/** API-DOC
 * @swagger
 * /api/v1.0.0/autotests:
 *  post:
 *    tags:
 *    - Auto Test  
 *    summary: Registra un nuevo auto test
 *    description: Registra un nuevo auto test
 *    parameters:
 *      - name: body
 *        in: body
 *        description: Datos del contenido
 *        required:
 *          - id_tramite
 *          - id_tipo_licencia
 *        schema:
 *          type: object
 *          properties:
 *            id_tramite:
 *              type: integer
 *              example: 1
 *            id_tipo_licencia:
 *              type: integer
 *              example: 1
 *    responses:
 *      '200':
 *         description: Consulta exitosa
 */

api.post("/autotests",
  [ md_auth.ensureAuth, partialResponse()],
  autoTestController.store
);


 /**
 * @swagger
 * /api/v1.0.0/autotests/{id}:
 *   put:
 *     tags:
 *     - Auto Test   
 *     summary: Actualiza un autotest.
 *     description: Actualiza un autotest.
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id del autotest a actualizar.
 *         type: integer
 *       - name: body
 *         in: body
 *         description: Datos del auto test
 *         required:
 *           - id_estado
 *         schema:
 *           type: object
 *           properties:
 *             id_estado:
 *               type: integer
 *               example: 1
 *             acertividad:
 *               type: number
 *               example: 7.8
 *             tiempo_ini:
 *               type: string
 *               example: '2022-06-06 12:43:42'
 *     responses:
 *       200:
 *         description: Consulta exitosa
*/

api.put("/autotests/:id",
  [ md_auth.ensureAuth ,partialResponse()],
  autoTestController.update
);


module.exports = api;

