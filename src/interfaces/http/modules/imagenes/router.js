"use strict";

const express = require("express");
const api = express.Router();
const md_auth = require("../../../../middlewares/authenticated");
//const apiMiddleware = require("../../../../middlewares/api");
const partialResponse = require("express-partial-response");
const imagenesController = require("./index");
const uploadImgs = require("../../../../middlewares/uploadImgs");
const updateImgs = require("../../../../middlewares/updateImgs");


/** API-DOC
 * @swagger
 * /api/v1.0.0/imagenes:
 *   get:
 *     tags:
 *     - Imagenes
 *     summary: Retorna una lista de imagenes
 *     description: Retorna una lista de imagenes
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: fields 
 *         in: query
 *         description: PartialResponce - Ej. results(id)
 *         required: false
 *         type: string
 *       - name: limit
 *         in: query
 *         description: cantidad a obtener  - default 30 (-1 no limita)
 *         required: false
 *         type: integer
 *       - name: offset
 *         in: query
 *         description: cantidad a saltear - default 0
 *         required: false
 *         type: integer
 *       - name: order
 *         in: query
 *         description: campo por el cual ordenar - default id
 *         required: false
 *         type: string
 *       - name: sort
 *         in: query
 *         description: tipo de ordenamiento (asc o desc) - default asc
 *         required: false
 *         type: string
 *       - name: q
 *         in: query
 *         description: filtro aplicado a nombre o descripcion (like %q%) - Ej. amite
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Consulta exitosa
 */
api.get("/imagenes",
  [ md_auth.ensureAuth, partialResponse()],
  imagenesController.index
);


 /**
 * @swagger
 * /api/v1.0.0/imagenes/{id}:
 *   get:
 *     tags:
 *     - Imagenes   
 *     summary: Retorna una imagen.
 *     description: Retorna una imagen.
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id de la imagen a obtener.
 *         type: integer
 *       - name: fields 
 *         in: query
 *         description: PartialResponce - Ej. id
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Consulta exitosa
*/
api.get("/imagenes/:id",
  [ md_auth.ensureAuth ,partialResponse()],
  imagenesController.show
);


/** API-DOC
 * @swagger
 * /api/v1.0.0/imagenes:
 *  post:
 *    tags:
 *    - Imagenes  
 *    summary: Registra nueva imagen
 *    description: Registra nueva imagen
 *    consumes:
 *      - multipart/form-data
 *    parameters:
 *      - name: file
 *        in: formData
 *        type: file
 *        description: archivo imagen.
 *        x-mimetype: image/*
 *      - name: nombre
 *        in: formData
 *        description: nombre de la imagen
 *      - name: descripcion
 *        in: formData
 *        description: descripcion de la imagen
 *    responses:
 *      '200':
 *         description: Consulta exitosa
 */
api.post("/imagenes",
  [ md_auth.ensureAuth, uploadImgs.single("file") ,partialResponse()],
  imagenesController.upload
);

 /**
 * @swagger
 * /api/v1.0.0/imagenes/{id}:
 *   put:
 *     tags:
 *     - Imagenes   
 *     summary: Actualiza una imagen.
 *     description: Actualiza una imagen.
 *     consumes:
 *       - multipart/form-data
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id de la imagen.
 *         type: integer
 *       - name: file
 *         in: formData
 *         type: file
 *         description: archivo imagen.
 *         x-mimetype: image/*
 *       - name: nombre
 *         in: formData
 *         description: nombre de la imagen
 *       - name: descripcion
 *         in: formData
 *         description: descripcion de la imagen
 *     responses:
 *       200:
 *         description: Consulta exitosa
*/

api.put("/imagenes/:id",
  [ md_auth.ensureAuth, updateImgs.single("file") ,partialResponse()],
  imagenesController.update
);

 /**
 * @swagger
 * /api/v1.0.0/imagenes/{id}:
 *   delete:
 *     tags:
 *     - Imagenes   
 *     summary: Elimina una imagen.
 *     description: Elimina una imagen.
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id de la imagen a eliminar.
 *         type: integer
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: integer
 *           description: cantidad de imagenes borrados.
 *           example: 1
*/
api.delete("/imagenes/:id",
  [ md_auth.ensureAuth ,partialResponse()],
  imagenesController.destroy
);

module.exports = api;

