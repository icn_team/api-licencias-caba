"use strict";
// const auth = require("../../../../app/auth");
const bcrypt = require('bcrypt');
const users = require("../../../../modules/users");


async function register(req, res, next){
  try {
    const result = await users.create(req.body);
    res.status(200).send(result);
  }
  catch (ex) {
    next(ex);
  }
}


async function login(req, res, next) {
  try {
    const result = await users.login(req.body);
    res.status(200).json(result);
  }
  catch (ex) {
    next(ex);
  }
}


module.exports = {
  login,
  register,
};
