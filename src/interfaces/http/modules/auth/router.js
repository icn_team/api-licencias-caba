"use strict";

const express = require("express");
const api = express.Router();
//const md_auth = require("../../../../middlewares/authenticated");
//const apiMiddleware = require("../../../../middlewares/api");
const partialResponse = require("express-partial-response");
const authController = require("./index");


/** API-DOC
 * @swagger
 * /api/v1.0.0/auth/register:
 *  post:
 *    tags:
 *    - Autentificación  
 *    summary: Registra un nuevo usuario
 *    description: Registra a un usuario
 *    parameters:
 *      - name: body
 *        in: body
 *        description: datos del usuario
 *        required:
 *          - nombre
 *          - apellido
 *          - dni
 *          - email
 *          - password
 *        schema:
 *          type: object
 *          properties:
 *            nombre:
 *              type: string
 *            apellido:
 *              type: string
 *            dni:
 *              type: string
 *            email:
 *              type: string
 *            password:
 *              type: string
 *    responses:
 *      '200':
 *        description: A successful response
 */
api.post("/auth/register",
  [partialResponse()],
  authController.register
);


/** API-DOC
 * @swagger
 * /api/v1.0.0/auth/login:
 *  post:
 *    tags:
 *    - Autentificación  
 *    summary: Loguea a un usuario registrado
 *    description: retorna un token de usuario
 *    parameters:
 *      - name: body
 *        in: body
 *        description: email y password del usuario
 *        required:
 *          - email
 *          - password
 *        schema:
 *          type: object
 *          properties:
 *            email:
 *              type: string
 *            password:
 *              type: string
 *    responses:
 *      '200':
 *        description: A successful response
 */
api.post("/auth/login",
  [partialResponse()],
  authController.login
);

module.exports = api;