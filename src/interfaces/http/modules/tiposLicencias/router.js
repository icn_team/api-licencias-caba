"use strict";

const express = require("express");
const api = express.Router();
const md_auth = require("../../../../middlewares/authenticated");
//const apiMiddleware = require("../../../../middlewares/api");
const partialResponse = require("express-partial-response");
const tiposLicenciasController = require("./index");


/** API-DOC
 * @swagger
 * /api/v1.0.0/tipos-licencias:
 *   get:
 *     tags:
 *     - Tipos Licencias 
 *     summary: Retorna una lista de tipos licencias
 *     description: Retorna una lista de tipos licencias
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: fields 
 *         in: query
 *         description: PartialResponce - Ej. results(id)
 *         required: false
 *         type: string
 *       - name: limit
 *         in: query
 *         description: cantidad a obtener  - default 30 (-1 no limita)
 *         required: false
 *         type: integer
 *       - name: offset
 *         in: query
 *         description: cantidad a saltear - default 0
 *         required: false
 *         type: integer
 *       - name: order
 *         in: query
 *         description: campo por el cual ordenar - default id
 *         required: false
 *         type: string
 *       - name: sort
 *         in: query
 *         description: tipo de ordenamiento (asc o desc) - default asc
 *         required: false
 *         type: string
 *       - name: q
 *         in: query
 *         description: filtro aplicado a nombre o descripcion (like %q%) - Ej. amite
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: object
 *           properties:
 *             metadata:
 *               type: object
 *               properties:
 *                 resultset:
 *                   type: object
 *                   properties:
 *                     count:
 *                       type: integer
 *                       example: 1
 *                     offset:
 *                       type: integer
 *                       example: 0 
 *                     limit:
 *                       type: integer
 *                       example: 30
 *             results:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   id:
 *                     type: integer
 *                     description: id de tipo licencia.
 *                     example: 1
 *                   nombre:
 *                     type: string
 *                     description: El nombre del tipo licencia.
 *                     example: Tipo licencia 1
 *                   descripcion:
 *                     type: string
 *                     description: La descripcion del tipo licencia.
 *                     example: Descripcion Tipo licencia 1
 *                   modulos:
 *                     type: array
 *                     items:
 *                       type: object
 *                       properties:
 *                         id:
 *                           type: integer
 *                           description: id de modulo.
 *                           example: 1
 *                         nombre:
 *                           type: string
 *                           description: El nombre del modulo.
 *                           example: Modulo 1
 *                         descripcion:
 *                           type: string
 *                           description: La descripcion del modulo.
 *                           example: Descripcion Modulo 1
 *                         orden:
 *                           type: integer
 *                           description: El orden.
 *                           example: 1
 */
api.get("/tipos-licencias",
  [ md_auth.ensureAuth, partialResponse()],
  tiposLicenciasController.index
);


 /**
 * @swagger
 * /api/v1.0.0/tipos-licencias/{id}:
 *   get:
 *     tags:
 *     - Tipos Licencias   
 *     summary: Retorna un tipo licencia.
 *     description: Retorna un tipo licencia.
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id del tipo licencia a obtener.
 *         type: integer
 *       - name: fields 
 *         in: query
 *         description: PartialResponce - Ej. id
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: id de tipo licencia.
 *               example: 1
 *             nombre:
 *               type: string
 *               description: El nombre del tipo licencia.
 *               example: tipo licencia 1
 *             descripcion:
 *               type: string
 *               description: La descripcion del tipo licencia.
 *               example: descripcion tipo licencia 1
 *             modulos:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   id:
 *                     type: integer
 *                     description: id de modulo.
 *                     example: 1
 *                   nombre:
 *                     type: string
 *                     description: El nombre del modulo.
 *                     example: Modulo 1
 *                   descripcion:
 *                     type: string
 *                     description: La descripcion del modulo.
 *                     example: Descripcion Modulo 1
 *                   orden:
 *                     type: integer
 *                     description: El orden.
 *                     example: 1
*/
api.get("/tipos-licencias/:id",
  [ md_auth.ensureAuth ,partialResponse()],
  tiposLicenciasController.show
);


/** API-DOC
 * @swagger
 * /api/v1.0.0/tipos-licencias:
 *  post:
 *    tags:
 *    - Tipos Licencias  
 *    summary: Registra un nuevo tipo licencia
 *    description: Registra un nuevo tipo licencia
 *    parameters:
 *      - name: body
 *        in: body
 *        description: Datos del tipo licencia
 *        required:
 *          - nombre
 *          - descripcion
 *          - modulos
 *        schema:
 *          type: object
 *          properties:
 *            nombre:
 *              type: string
 *            descripcion:
 *              type: string
 *            modulos:
 *              type: array
 *              items:
 *                type: object
 *                properties:
 *                  id:
 *                    type: integer
 *                    description: id de modulo.
 *                    example: 1
 *                  orden:
 *                    type: integer
 *                    description: El orden.
 *                    example: 1
 *    responses:
 *      '200':
 *         description: Consulta exitosa
 *         schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: id de tipo licencia.
 *               example: 1
 *             nombre:
 *               type: string
 *               description: El nombre del tipo licencia.
 *               example: tipo licencia 1
 *             descripcion:
 *               type: string
 *               description: La descripcion del tipo licencia.
 *               example: descripcion tipo licencia 1
 *             modulos:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   id:
 *                     type: integer
 *                     description: id de modulo.
 *                     example: 1
 *                   nombre:
 *                     type: string
 *                     description: El nombre del modulo.
 *                     example: Modulo 1
 *                   descripcion:
 *                     type: string
 *                     description: La descripcion del modulo.
 *                     example: Descripcion Modulo 1
 *                   orden:
 *                     type: integer
 *                     description: El orden.
 *                     example: 1
 */
api.post("/tipos-licencias",
  [ md_auth.ensureAuth, partialResponse()],
  tiposLicenciasController.store
);


 /**
 * @swagger
 * /api/v1.0.0/tipos-licencias/{id}:
 *   put:
 *     tags:
 *     - Tipos Licencias   
 *     summary: Actualiza un tipo licencia.
 *     description: Actualiza un tipo licencia.
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id del tipo licencia a actualizar.
 *         type: integer
 *       - name: body
 *         in: body
 *         description: Datos del tipo licencia
 *         required:
 *          - nombre
 *          - descripcion
 *          - modulos
 *         schema:
 *           type: object
 *           properties:
 *             nombre:
 *               type: string
 *             descripcion:
 *               type: string
 *             modulos:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   id:
 *                     type: integer
 *                     description: id de modulo.
 *                     example: 1
 *                   orden:
 *                     type: integer
 *                     description: El orden.
 *                     example: 1
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: id de tipo licencia.
 *               example: 1
 *             nombre:
 *               type: string
 *               description: El nombre del tipo licencia.
 *               example: tipo licencia 1
 *             descripcion:
 *               type: string
 *               description: La descripcion del tipo licencia.
 *               example: descripcion tipo licencia 1
 *             modulos:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   id:
 *                     type: integer
 *                     description: id de modulo.
 *                     example: 1
 *                   nombre:
 *                     type: string
 *                     description: El nombre del modulo.
 *                     example: Modulo 1
 *                   descripcion:
 *                     type: string
 *                     description: La descripcion del modulo.
 *                     example: Descripcion Modulo 1
 *                   orden:
 *                     type: integer
 *                     description: El orden.
 *                     example: 1
*/
api.put("/tipos-licencias/:id",
  [ md_auth.ensureAuth ,partialResponse()],
  tiposLicenciasController.update
);

 /**
 * @swagger
 * /api/v1.0.0/tipos-licencias/{id}:
 *   delete:
 *     tags:
 *     - Tipos Licencias   
 *     summary: Elimina un tipo licencia.
 *     description: Elimina un tipo licencia.
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id del tipo licencia a eliminar.
 *         type: integer
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: integer
 *           description: cantidad de tipos licencias borrados.
 *           example: 1
*/
api.delete("/tipos-licencias/:id",
  [ md_auth.ensureAuth ,partialResponse()],
  tiposLicenciasController.destroy
);

 /**
 * @swagger
 * /api/v1.0.0/tipos-licencias:
 *   delete:
 *     tags:
 *     - Tipos Licencias   
 *     summary: Elimina todos los tipos licencias.
 *     description: Elimina todos los tipos licencias.
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: integer
 *           description: cantidad de tipos licencias borrados.
 *           example: 12
*/
api.delete("/tipos-licencias",
  [ md_auth.ensureAuth ,partialResponse()],
  tiposLicenciasController.destroyAll
);

module.exports = api;

