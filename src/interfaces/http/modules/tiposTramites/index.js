"use strict";
const tiposTramites = require("../../../../modules/tiposTramites");

async function index(req, res, next){
  try {
    const results = await tiposTramites.index(req);
    return res.status(200).json(results)

  }
  catch (ex) {
    next(ex);
  }
}

async function show(req, res, next){
  try {
    const result = await tiposTramites.show(req.params.id);
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}

async function store(req, res, next){
  try {
    const result = await tiposTramites.store(req.body);
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}

async function update(req, res, next){
  try {
    const result = await tiposTramites.update(req.params.id,req.body);
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}

async function destroy(req, res, next){
  try {
    const result = await tiposTramites.destroy(req.params.id);
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}

async function destroyAll(req, res, next){
  try {
    const result = await tiposTramites.destroyAll();
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}

module.exports = {
  index,
  show,
  store,
  update,
  destroy,
  destroyAll
};
