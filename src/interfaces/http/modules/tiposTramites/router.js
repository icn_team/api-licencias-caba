"use strict";

const express = require("express");
const api = express.Router();
const md_auth = require("../../../../middlewares/authenticated");
//const apiMiddleware = require("../../../../middlewares/api");
const partialResponse = require("express-partial-response");
const tiposTramitesController = require("./index");


/** API-DOC
 * @swagger
 * /api/v1.0.0/tipos-tramites:
 *   get:
 *     tags:
 *     - Tipos Tramites 
 *     summary: Retorna una lista de tipos tramites
 *     description: Retorna una lista de tipos tramites
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: fields 
 *         in: query
 *         description: PartialResponce - Ej. results(id)
 *         required: false
 *         type: string
 *       - name: limit
 *         in: query
 *         description: cantidad a obtener  - default 30 (-1 no limita)
 *         required: false
 *         type: integer
 *       - name: offset
 *         in: query
 *         description: cantidad a saltear - default 0
 *         required: false
 *         type: integer
 *       - name: order
 *         in: query
 *         description: campo por el cual ordenar - default id
 *         required: false
 *         type: string
 *       - name: sort
 *         in: query
 *         description: tipo de ordenamiento (asc o desc) - default asc
 *         required: false
 *         type: string
 *       - name: q
 *         in: query
 *         description: filtro aplicado a nombre o descripcion (like %q%) - Ej. amite
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: object
 *           properties:
 *             metadata:
 *               type: object
 *               properties:
 *                 resultset:
 *                   type: object
 *                   properties:
 *                     count:
 *                       type: integer
 *                       example: 1
 *                     offset:
 *                       type: integer
 *                       example: 0 
 *                     limit:
 *                       type: integer
 *                       example: 30
 *             results:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   id:
 *                     type: integer
 *                     description: id de tipo tramite.
 *                     example: 1
 *                   nombre:
 *                     type: string
 *                     description: El nombre del tipo tramite.
 *                     example: Tipo tramite 1
 *                   descripcion:
 *                     type: string
 *                     description: La descripcion del tipo tramite.
 *                     example: Descripcion Tipo tramite 1
 */
api.get("/tipos-tramites",
  [ md_auth.ensureAuth, partialResponse()],
  tiposTramitesController.index
);


 /**
 * @swagger
 * /api/v1.0.0/tipos-tramites/{id}:
 *   get:
 *     tags:
 *     - Tipos Tramites   
 *     summary: Retorna un tipo tramite.
 *     description: Retorna un tipo tramite.
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id del tipo tramite a obtener.
 *         type: integer
 *       - name: fields 
 *         in: query
 *         description: PartialResponce - Ej. id
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: id de tipo tramite.
 *               example: 1
 *             nombre:
 *               type: string
 *               description: El nombre del tipo tramite.
 *               example: tipo tramite 1
 *             descripcion:
 *               type: string
 *               description: La descripcion del tipo tramite.
 *               example: descripcion tipo tramite 1
*/
api.get("/tipos-tramites/:id",
  [ md_auth.ensureAuth ,partialResponse()],
  tiposTramitesController.show
);


/** API-DOC
 * @swagger
 * /api/v1.0.0/tipos-tramites:
 *  post:
 *    tags:
 *    - Tipos Tramites  
 *    summary: Registra un nuevo tipo tramite
 *    description: Registra un nuevo tipo tramite
 *    parameters:
 *      - name: body
 *        in: body
 *        description: Datos del tipo tramite
 *        required:
 *          - nombre
 *          - descripcion
 *        schema:
 *          type: object
 *          properties:
 *            nombre:
 *              type: string
 *            descripcion:
 *              type: string
 *    responses:
 *      '200':
 *         description: Consulta exitosa
 *         schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: id de tipo tramite.
 *               example: 1
 *             nombre:
 *               type: string
 *               description: El nombre del tipo tramite.
 *               example: tipo tramite 1
 *             descripcion:
 *               type: string
 *               description: La descripcion del tipo tramite.
 *               example: descripcion tipo tramite 1
 */
api.post("/tipos-tramites",
  [ md_auth.ensureAuth, partialResponse()],
  tiposTramitesController.store
);


 /**
 * @swagger
 * /api/v1.0.0/tipos-tramites/{id}:
 *   put:
 *     tags:
 *     - Tipos Tramites   
 *     summary: Actualiza un tipo tramite.
 *     description: Actualiza un tipo tramite.
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id del tipo tramite a actualizar.
 *         type: integer
 *       - name: body
 *         in: body
 *         description: Datos del tipo tramite
 *         required:
 *          - nombre
 *          - descripcion
 *         schema:
 *          type: object
 *          properties:
 *            nombre:
 *              type: string
 *            descripcion:
 *              type: string
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: id de tipo tramite.
 *               example: 1
 *             nombre:
 *               type: string
 *               description: El nombre del tipo tramite.
 *               example: tipo tramite 1
 *             descripcion:
 *               type: string
 *               description: La descripcion del tipo tramite.
 *               example: descripcion tipo tramite 1
*/
api.put("/tipos-tramites/:id",
  [ md_auth.ensureAuth ,partialResponse()],
  tiposTramitesController.update
);

 /**
 * @swagger
 * /api/v1.0.0/tipos-tramites/{id}:
 *   delete:
 *     tags:
 *     - Tipos Tramites   
 *     summary: Elimina un tipo tramite.
 *     description: Elimina un tipo tramite.
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id del tipo tramite a eliminar.
 *         type: integer
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: integer
 *           description: cantidad de tipos tramites borrados.
 *           example: 1
*/
api.delete("/tipos-tramites/:id",
  [ md_auth.ensureAuth ,partialResponse()],
  tiposTramitesController.destroy
);

 /**
 * @swagger
 * /api/v1.0.0/tipos-tramites:
 *   delete:
 *     tags:
 *     - Tipos Tramites   
 *     summary: Elimina todos los tipos tramites.
 *     description: Elimina todos los tipos tramites.
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: integer
 *           description: cantidad de tipos tramites borrados.
 *           example: 12
*/
api.delete("/tipos-tramites",
  [ md_auth.ensureAuth ,partialResponse()],
  tiposTramitesController.destroyAll
);

module.exports = api;

