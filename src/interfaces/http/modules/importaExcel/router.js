"use strict";

const express = require("express");
const api = express.Router();
//const md_auth = require("../../../../middlewares/authenticated");
//const apiMiddleware = require("../../../../middlewares/api");
const partialResponse = require("express-partial-response");
const importaExcelController = require("./index");


/** API-DOC
 * @swagger
 * /api/v1.0.0/excel/procesar:
 *  post:
 *    tags:
 *    - Importar Preguntas  
 *    summary: Procesa las preguntas del excel
 *    description: Procesa las preguntas del excel
 *    responses:
 *      '200':
 *        description: A successful response
 */
api.post("/excel/procesar",
  [partialResponse()],
  importaExcelController.procesar
);

module.exports = api;