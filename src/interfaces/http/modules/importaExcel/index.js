"use strict";
const importaExcel = require("../../../../modules/importaExcel");


async function procesar(req, res, next){
  try {
    const result = await importaExcel.procesar(req.body);
    return res.status(200).json({
      data: result,
      status : 200
    })

   //res.status(200).send(result);
  }
  catch (ex) {
    next(ex);
  }
}

module.exports = {
  procesar,
};
