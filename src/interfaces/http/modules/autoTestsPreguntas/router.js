"use strict";

const express = require("express");
const api = express.Router();
const md_auth = require("../../../../middlewares/authenticated");
//const apiMiddleware = require("../../../../middlewares/api");
const partialResponse = require("express-partial-response");
const autoTestPreguntasController = require("./index");

 /**
 * @swagger
 * /api/v1.0.0/autotests/{id_auto_test}/preguntas/{id_pregunta}:
 *   put:
 *     tags:
 *     - Auto Test Preguntas
 *     summary: Actualiza el resultado de una rta para una pregunta de un autotest.
 *     description: Actualiza el resultado de una rta para una pregunta de un autotest.
 *     parameters:
 *       - in: path
 *         name: id_auto_test
 *         required: true
 *         description: El id del autotest.
 *         type: integer
 *       - in: path
 *         name: id_pregunta
 *         required: true
 *         description: El id de la pregunta.
 *         type: integer
 *       - name: body
 *         in: body
 *         description: Resultado de la respuesta
 *         required:
 *           - rta
 *         properties:
 *           rta:
 *             type: integer
 *             example: 1
 *     responses:
 *       200:
 *         description: Consulta exitosa
*/



api.put("/autotests/:id_auto_test/preguntas/:id_pregunta",
  [ md_auth.ensureAuth ,partialResponse()],
  autoTestPreguntasController.update
);


module.exports = api;
