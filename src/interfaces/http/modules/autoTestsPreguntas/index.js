"use strict";
const autoTestsPreguntas = require("../../../../modules/autoTestsPreguntas");

async function update(req, res, next){
  try {
    const result = await autoTestsPreguntas.update(req.params.id_auto_test,req.params.id_pregunta,req.body);
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}


module.exports = {
  update
};
