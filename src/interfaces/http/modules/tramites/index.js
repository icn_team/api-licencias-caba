"use strict";
const tramites = require("../../../../modules/tramites");

async function index(req, res, next){
  try {
    const results = await tramites.index(req);
    return res.status(200).json(results)

  }
  catch (ex) {
    next(ex);
  }
}

async function show(req, res, next){
  try {
    const result = await tramites.show(req.params.id);
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}

async function store(req, res, next){
  try {
    const result = await tramites.store(req.body);
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}

async function update(req, res, next){
  try {
    const result = await tramites.update(req.params.id,req.body);
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}

async function destroy(req, res, next){
  try {
    const result = await tramites.destroy(req.params.id);
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}

async function destroyAll(req, res, next){
  try {
    const result = await tramites.destroyAll();
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}

async function sincrono(req, res, next){
  try {
    const result = await tramites.sincrono(req.params.id,req.body);
    return res.status(200).json(result)
  }
  catch (ex) {
    next(ex);
  }
}

module.exports = {
  index,
  show,
  store,
  update,
  destroy,
  destroyAll
};
