"use strict";

const express = require("express");
const api = express.Router();
const md_auth = require("../../../../middlewares/authenticated");
//const apiMiddleware = require("../../../../middlewares/api");
const partialResponse = require("express-partial-response");
const tramitesController = require("./index");


/** API-DOC
 * @swagger
 * /api/v1.0.0/tramites:
 *   get:
 *     tags:
 *     - Tramites 
 *     summary: Retorna una lista de tramites
 *     description: Retorna una lista de tramites
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: fields 
 *         in: query
 *         description: PartialResponce - Ej. results(id)
 *         required: false
 *         type: string
 *       - name: limit
 *         in: query
 *         description: cantidad a obtener  - default 30 (-1 no limita)
 *         required: false
 *         type: integer
 *       - name: offset
 *         in: query
 *         description: cantidad a saltear - default 0
 *         required: false
 *         type: integer
 *       - name: order
 *         in: query
 *         description: campo por el cual ordenar - default id
 *         required: false
 *         type: string
 *       - name: sort
 *         in: query
 *         description: tipo de ordenamiento (asc o desc) - default asc
 *         required: false
 *         type: string
 *       - name: id_user
 *         in: query
 *         description: filtra tramites de un usuario segun su id - default tramites de todos los usuarios
 *         required: false
 *         type: integer
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: object
 *           properties:
 *             metadata:
 *               type: object
 *               properties:
 *                 resultset:
 *                   type: object
 *                   properties:
 *                     count:
 *                       type: integer
 *                       example: 1
 *                     offset:
 *                       type: integer
 *                       example: 0 
 *                     limit:
 *                       type: integer
 *                       example: 30
 *             results:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   id:
 *                     type: integer
 *                     description: id de tramite.
 *                     example: 1
 *                   id_tipo_tramite:
 *                     type: integer
 *                     description: id de tipo tramite.
 *                     example: 1
 *                   id_user:
 *                     type: integer
 *                     description: id de usuario.
 *                     example: 693
 *                   tipos_licencias:
 *                     type: array
 *                     description: tipos licencias.
 *                     example: []
 *                   id_estado_tramite:
 *                     type: integer
 *                     description: id de estado tramite.
 *                     example: 1
 *                   fecha_curso_sincrono:
 *                     type: string
 *                     description: fecha del curso sincrono.
 *                     example: '2022-06-19 13:23:56'
 *                   presente_curso_sincrono:
 *                     type: integer
 *                     description: presente curso sincrono.
 *                     example: 1
 *                   calificacion_curso_sincrono:
 *                     type: integer
 *                     description: calificacion curso sincrono.
 *                     example: 8
 */
api.get("/tramites",
  [ md_auth.ensureAuth, partialResponse()],
  tramitesController.index
);


 /**
 * @swagger
 * /api/v1.0.0/tramites/{id}:
 *   get:
 *     tags:
 *     - Tramites   
 *     summary: Retorna un tramite.
 *     description: Retorna un tramite.
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id del tramite a obtener.
 *         type: integer
 *       - name: fields 
 *         in: query
 *         description: PartialResponce - Ej. id
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: id de tramite.
 *               example: 1
 *             id_tipo_tramite:
 *               type: integer
 *               description: id de tipo tramite.
 *               example: 1
 *             id_user:
 *               type: integer
 *               description: id de usuario.
 *               example: 693
 *             tipos_licencias:
 *               type: array
 *               description: tipos licencias.
 *               example: []
 *             id_estado_tramite:
 *               type: integer
 *               description: id de estado tramite.
 *               example: 1
 *             fecha_curso_sincrono:
 *               type: string
 *               description: fecha del curso sincrono.
 *               example: '2022-06-19 13:23:56'
 *             presente_curso_sincrono:
 *               type: integer
 *               description: presente curso sincrono.
 *               example: 1
 *             calificacion_curso_sincrono:
 *               type: integer
 *               description: calificacion curso sincrono.
 *               example: 8
*/
api.get("/tramites/:id",
  [ md_auth.ensureAuth ,partialResponse()],
  tramitesController.show
);


/** API-DOC
 * @swagger
 * /api/v1.0.0/tramites:
 *  post:
 *    tags:
 *    - Tramites  
 *    summary: Registra un nuevo tramite
 *    description: Registra un nuevo tramite
 *    parameters:
 *      - name: body
 *        in: body
 *        description: Datos del tramite
 *        required:
 *          - id_tipo_tramite
 *          - id_user
 *          - tipos_licencias
 *          - id_estado_tramite
 *          - fecha_curso_sincrono
 *          - presente_curso_sincrono
 *          - calificacion_curso_sincrono
 *        schema:
 *          type: object
 *          properties:
 *            id_tipo_tramite:
 *              type: integer
 *              example: 1
 *            id_user:
 *              type: integer
 *              example: 693
 *            tipos_licencias:
 *              type: array
 *              items:
 *                type: integer
 *                example: 1
 *            id_estado_tramite:
 *              type: integer
 *              example: 1
 *            fecha_curso_sincrono:
 *              type: string
 *              example: '2022-06-19 13:45:42'
 *            presente_curso_sincrono:
 *              type: integer
 *              example: 1
 *            calificacion_curso_sincrono:
 *              type: integer
 *              example: 7
 *    responses:
 *      '200':
 *         description: Consulta exitosa
 *         schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: id de tramite.
 *               example: 1
 *             id_tipo_tramite:
 *               type: integer
 *               description: id de tipo tramite.
 *               example: 1
 *             id_user:
 *               type: integer
 *               description: id de usuario.
 *               example: 693
 *             id_tipo_licencia:
 *               type: integer
 *               description: id de tipo licencia.
 *               example: 1
 *             id_estado_tramite:
 *               type: integer
 *               description: id de estado tramite.
 *               example: 1
 *             fecha_curso_sincrono:
 *               type: string
 *               description: fecha del curso sincrono.
 *               example: '2022-06-19 13:23:56'
 *             presente_curso_sincrono:
 *               type: integer
 *               description: presente curso sincrono.
 *               example: 1
 *             calificacion_curso_sincrono:
 *               type: integer
 *               description: calificacion curso sincrono.
 *               example: 8
 */
api.post("/tramites",
  [ md_auth.ensureAuth, partialResponse()],
  tramitesController.store
);


 /**
 * @swagger
 * /api/v1.0.0/tramites/{id}:
 *   put:
 *     tags:
 *     - Tramites   
 *     summary: Actualiza un tramite.
 *     description: Actualiza un tramite.
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id del tramite a actualizar.
 *         type: integer
 *       - name: body
 *         in: body
 *         description: Datos del tramite
 *         required:
 *          - id_tipo_tramite
 *          - id_user
 *          - tipos_licencias
 *          - id_estado_tramite
 *          - fecha_curso_sincrono
 *          - presente_curso_sincrono
 *          - calificacion_curso_sincrono
 *         schema:
 *          type: object
 *          properties:
 *            id_tipo_tramite:
 *              type: integer
 *              example: 1
 *            id_user:
 *              type: integer
 *              example: 693
 *            tipos_licencias:
 *              type: array
 *              items:
 *                type: integer
 *                example: 1
 *            id_estado_tramite:
 *              type: integer
 *              example: 1
 *            fecha_curso_sincrono:
 *              type: string
 *              example: '2022-06-19 13:45:42'
 *            presente_curso_sincrono:
 *              type: integer
 *              example: 1
 *            calificacion_curso_sincrono:
 *              type: integer
 *              example: 7
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: id de tramite.
 *               example: 1
 *             id_tipo_tramite:
 *               type: integer
 *               description: id de tipo tramite.
 *               example: 1
 *             id_user:
 *               type: integer
 *               description: id de usuario.
 *               example: 693
 *             id_tipo_licencia:
 *               type: integer
 *               description: id de tipo licencia.
 *               example: 1
 *             id_estado_tramite:
 *               type: integer
 *               description: id de estado tramite.
 *               example: 1
 *             fecha_curso_sincrono:
 *               type: string
 *               description: fecha del curso sincrono.
 *               example: '2022-06-19 13:23:56'
 *             presente_curso_sincrono:
 *               type: integer
 *               description: presente curso sincrono.
 *               example: 1
 *             calificacion_curso_sincrono:
 *               type: integer
 *               description: calificacion curso sincrono.
 *               example: 8
*/
api.put("/tramites/:id",
  [ md_auth.ensureAuth ,partialResponse()],
  tramitesController.update
);

 /**
 * @swagger
 * /api/v1.0.0/tramites/{id}:
 *   delete:
 *     tags:
 *     - Tramites   
 *     summary: Elimina un tramite.
 *     description: Elimina un tramite.
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id del tramite a eliminar.
 *         type: integer
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: integer
 *           description: cantidad de tramites borrados.
 *           example: 1
*/
api.delete("/tramites/:id",
  [ md_auth.ensureAuth ,partialResponse()],
  tramitesController.destroy
);

 /**
 * @swagger
 * /api/v1.0.0/tramites:
 *   delete:
 *     tags:
 *     - Tramites   
 *     summary: Elimina todos los tramites.
 *     description: Elimina todos los tramites.
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: integer
 *           description: cantidad de tramites borrados.
 *           example: 12
*/
api.delete("/tramites",
  [ md_auth.ensureAuth ,partialResponse()],
  tramitesController.destroyAll
);

module.exports = api;

