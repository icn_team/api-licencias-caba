"use strict";

const express = require("express");
const api = express.Router();
const md_auth = require("../../../../middlewares/authenticated");
//const apiMiddleware = require("../../../../middlewares/api");
const partialResponse = require("express-partial-response");
const modulosController = require("./index");


/** API-DOC
 * @swagger
 * /api/v1.0.0/modulos:
 *   get:
 *     tags:
 *     - Modulos 
 *     summary: Retorna una lista de modulos
 *     description: Retorna una lista de modulos
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: fields 
 *         in: query
 *         description: PartialResponce - Ej. results(id)
 *         required: false
 *         type: string
 *       - name: limit
 *         in: query
 *         description: cantidad a obtener  - default 30 (-1 no limita)
 *         required: false
 *         type: integer
 *       - name: offset
 *         in: query
 *         description: cantidad a saltear - default 0
 *         required: false
 *         type: integer
 *       - name: order
 *         in: query
 *         description: campo por el cual ordenar - default id
 *         required: false
 *         type: string
 *       - name: sort
 *         in: query
 *         description: tipo de ordenamiento (asc o desc) - default asc
 *         required: false
 *         type: string
 *       - name: q
 *         in: query
 *         description: filtro aplicado a nombre o descripcion (like %q%) - Ej. amite
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: object
 *           properties:
 *             metadata:
 *               type: object
 *               properties:
 *                 resultset:
 *                   type: object
 *                   properties:
 *                     count:
 *                       type: integer
 *                       example: 1
 *                     offset:
 *                       type: integer
 *                       example: 0 
 *                     limit:
 *                       type: integer
 *                       example: 30
 *             results:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   id:
 *                     type: integer
 *                     description: id de modulo.
 *                     example: 1
 *                   nombre:
 *                     type: string
 *                     description: El nombre del modulo.
 *                     example: Modulo 1
 *                   descripcion:
 *                     type: string
 *                     description: La descripcion del modulo.
 *                     example: Modulo perteneciente a videos
 *                   contenidos:
 *                     type: array
 *                     items:
 *                       type: object
 *                       properties:
 *                         id:
 *                           type: integer
 *                           description: id del contenido.
 *                           example: 1
 *                         nombre:
 *                           type: string
 *                           description: El nombre del contenio.
 *                           example: Contenido 1
 *                         data:
 *                           type: string
 *                           description: contenido.
 *                           example: contenido
 *                         id_tipo_contenido:
 *                           type: integer
 *                           description: El tipo de contenido.
 *                           example: 1
 *                         orden:
 *                           type: integer
 *                           description: El orden.
 *                           example: 1
 */
api.get("/modulos",
  [ md_auth.ensureAuth, partialResponse()],
  modulosController.index
);


 /**
 * @swagger
 * /api/v1.0.0/modulos/{id}:
 *   get:
 *     tags:
 *     - Modulos   
 *     summary: Retorna un modulo.
 *     description: Retorna un modulo.
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id del modulo a obtener.
 *         type: integer
 *       - name: fields 
 *         in: query
 *         description: PartialResponce - Ej. id
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: id de modulo.
 *               example: 1
 *             nombre:
 *               type: string
 *               description: El nombre del modulo.
 *               example: Modulo 1
 *             descripcion:
 *               type: string
 *               description: La descripcion del modulo.
 *               example: Modulo perteneciente a videos
 *             contenidos:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   id:
 *                     type: integer
 *                     description: id del contenido.
 *                     example: 1
 *                   nombre:
 *                     type: string
 *                     description: El nombre del contenio.
 *                     example: Contenido 1
 *                   data:
 *                     type: string
 *                     description: contenido.
 *                     example: contenido
 *                   id_tipo_contenido:
 *                     type: integer
 *                     description: El tipo de contenido.
 *                     example: 1
 *                   orden:
 *                     type: integer
 *                     description: El orden.
 *                     example: 1
*/
api.get("/modulos/:id",
  [ md_auth.ensureAuth ,partialResponse()],
  modulosController.show
);


/** API-DOC
 * @swagger
 * /api/v1.0.0/modulos:
 *  post:
 *    tags:
 *    - Modulos  
 *    summary: Registra un nuevo modulo
 *    description: Registra un nuevo modulo
 *    parameters:
 *      - name: body
 *        in: body
 *        description: Datos del modulo
 *        required:
 *          - nombre
 *          - descripcion
 *          - contenidos
 *        schema:
 *          type: object
 *          properties:
 *            nombre:
 *              type: string
 *            descripcion:
 *              type: string
 *            contenidos:
 *              type: array
 *              items:
 *                type: object
 *                properties:
 *                  id:
 *                    type: integer
 *                    description: id del contenido.
 *                    example: 1
 *                  orden:
 *                    type: integer
 *                    description: El orden.
 *                    example: 1
 *    responses:
 *      '200':
 *         description: Consulta exitosa
 *         schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: id de modulo.
 *               example: 1
 *             nombre:
 *               type: string
 *               description: El nombre del modulo.
 *               example: Modulo 1
 *             descripcion:
 *               type: string
 *               description: La descripcion del modulo.
 *               example: Modulo perteneciente a videos
 *             contenidos:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   id:
 *                     type: integer
 *                     description: id del contenido.
 *                     example: 1
 *                   nombre:
 *                     type: string
 *                     description: El nombre del contenio.
 *                     example: Contenido 1
 *                   data:
 *                     type: string
 *                     description: contenido.
 *                     example: contenido
 *                   id_tipo_contenido:
 *                     type: integer
 *                     description: El tipo de contenido.
 *                     example: 1
 *                   orden:
 *                     type: integer
 *                     description: El orden.
 *                     example: 1
 */
api.post("/modulos",
  [ md_auth.ensureAuth, partialResponse()],
  modulosController.store
);


 /**
 * @swagger
 * /api/v1.0.0/modulos/{id}:
 *   put:
 *     tags:
 *     - Modulos   
 *     summary: Actualiza un modulo.
 *     description: Actualiza un modulo.
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id del modulo a actualizar.
 *         type: integer
 *       - name: body
 *         in: body
 *         description: Datos del modulo
 *         required:
 *          - nombre
 *          - descripcion
 *          - contenidos
 *         schema:
 *          type: object
 *          properties:
 *            nombre:
 *              type: string
 *            descripcion:
 *              type: string
 *            contenidos:
 *              type: array
 *              items:
 *                type: object
 *                properties:
 *                  id:
 *                    type: integer
 *                    description: id del contenido.
 *                    example: 1
 *                  orden:
 *                    type: integer
 *                    description: El orden.
 *                    example: 1
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: id de modulo.
 *               example: 1
 *             nombre:
 *               type: string
 *               description: El nombre del modulo.
 *               example: Modulo 1
 *             descripcion:
 *               type: string
 *               description: La descripcion del modulo.
 *               example: Modulo perteneciente a videos
 *             contenidos:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   id:
 *                     type: integer
 *                     description: id del contenido.
 *                     example: 1
 *                   nombre:
 *                     type: string
 *                     description: El nombre del contenio.
 *                     example: Contenido 1
 *                   data:
 *                     type: string
 *                     description: contenido.
 *                     example: contenido
 *                   id_tipo_contenido:
 *                     type: integer
 *                     description: El tipo de contenido.
 *                     example: 1
 *                   orden:
 *                     type: integer
 *                     description: El orden.
 *                     example: 1
*/
api.put("/modulos/:id",
  [ md_auth.ensureAuth ,partialResponse()],
  modulosController.update
);

 /**
 * @swagger
 * /api/v1.0.0/modulos/{id}:
 *   delete:
 *     tags:
 *     - Modulos   
 *     summary: Elimina un modulo.
 *     description: Elimina un modulo.
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id del modulo a eliminar.
 *         type: integer
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: integer
 *           description: cantidad de modulos borrados.
 *           example: 1
*/
api.delete("/modulos/:id",
  [ md_auth.ensureAuth ,partialResponse()],
  modulosController.destroy
);

 /**
 * @swagger
 * /api/v1.0.0/modulos:
 *   delete:
 *     tags:
 *     - Modulos   
 *     summary: Elimina todos los modulos.
 *     description: Elimina todos los modulos.
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: integer
 *           description: cantidad de modulos borrados.
 *           example: 12
*/
api.delete("/modulos",
  [ md_auth.ensureAuth ,partialResponse()],
  modulosController.destroyAll
);

module.exports = api;

