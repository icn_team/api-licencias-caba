"use strict";

const express = require("express");
const api = express.Router();
const md_auth = require("../../../../middlewares/authenticated");
//const apiMiddleware = require("../../../../middlewares/api");
const partialResponse = require("express-partial-response");
const temasController = require("./index");


/** API-DOC
 * @swagger
 * /api/v1.0.0/temas:
 *   get:
 *     tags:
 *     - Temas 
 *     summary: Retorna una lista de temas
 *     description: Retorna una lista de temas
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: fields 
 *         in: query
 *         description: PartialResponce - Ej. results(id)
 *         required: false
 *         type: string
 *       - name: limit
 *         in: query
 *         description: cantidad a obtener  - default 30 (-1 no limita)
 *         required: false
 *         type: integer
 *       - name: offset
 *         in: query
 *         description: cantidad a saltear - default 0
 *         required: false
 *         type: integer
 *       - name: order
 *         in: query
 *         description: campo por el cual ordenar - default id
 *         required: false
 *         type: string
 *       - name: sort
 *         in: query
 *         description: tipo de ordenamiento (asc o desc) - default asc
 *         required: false
 *         type: string
 *       - name: q
 *         in: query
 *         description: filtro aplicado a nombre (like %q%) - Ej. amite
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: object
 *           properties:
 *             metadata:
 *               type: object
 *               properties:
 *                 resultset:
 *                   type: object
 *                   properties:
 *                     count:
 *                       type: integer
 *                       example: 1
 *                     offset:
 *                       type: integer
 *                       example: 0 
 *                     limit:
 *                       type: integer
 *                       example: 30
 *             results:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   id:
 *                     type: integer
 *                     description: id de tema.
 *                     example: 1
 *                   nombre:
 *                     type: string
 *                     description: El nombre del tema.
 *                     example: tema 1
 */
api.get("/temas",
  [ md_auth.ensureAuth, partialResponse()],
  temasController.index
);


 /**
 * @swagger
 * /api/v1.0.0/temas/{id}:
 *   get:
 *     tags:
 *     - Temas   
 *     summary: Retorna un tema.
 *     description: Retorna un tema.
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id del tema a obtener.
 *         type: integer
 *       - name: fields 
 *         in: query
 *         description: PartialResponce - Ej. id
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: id de tema.
 *               example: 1
 *             nombre:
 *               type: string
 *               description: El nombre del tema.
 *               example: tema 1
*/
api.get("/temas/:id",
  [ md_auth.ensureAuth ,partialResponse()],
  temasController.show
);


/** API-DOC
 * @swagger
 * /api/v1.0.0/temas:
 *  post:
 *    tags:
 *    - Temas  
 *    summary: Registra un nuevo tema
 *    description: Registra un nuevo tema
 *    parameters:
 *      - name: body
 *        in: body
 *        description: Datos del tema
 *        required:
 *          - nombre
 *        schema:
 *          type: object
 *          properties:
 *            nombre:
 *              type: string
 *    responses:
 *      '200':
 *         description: Consulta exitosa
 *         schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: id de tema.
 *               example: 1
 *             nombre:
 *               type: string
 *               description: El nombre del tema.
 *               example: tema 1
 */
api.post("/temas",
  [ md_auth.ensureAuth, partialResponse()],
  temasController.store
);


 /**
 * @swagger
 * /api/v1.0.0/temas/{id}:
 *   put:
 *     tags:
 *     - Temas   
 *     summary: Actualiza un tema.
 *     description: Actualiza un tema.
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id del tema a actualizar.
 *         type: integer
 *       - name: body
 *         in: body
 *         description: Datos del tema
 *         required:
 *          - nombre
 *         schema:
 *          type: object
 *          properties:
 *            nombre:
 *              type: string
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: id de tema.
 *               example: 1
 *             nombre:
 *               type: string
 *               description: El nombre del tema.
 *               example: tema 1
*/
api.put("/temas/:id",
  [ md_auth.ensureAuth ,partialResponse()],
  temasController.update
);

 /**
 * @swagger
 * /api/v1.0.0/temas/{id}:
 *   delete:
 *     tags:
 *     - Temas   
 *     summary: Elimina un tema.
 *     description: Elimina un tema.
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: El id del tema a eliminar.
 *         type: integer
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: integer
 *           description: cantidad de temas borrados.
 *           example: 1
*/
api.delete("/temas/:id",
  [ md_auth.ensureAuth ,partialResponse()],
  temasController.destroy
);

 /**
 * @swagger
 * /api/v1.0.0/temas:
 *   delete:
 *     tags:
 *     - Temas   
 *     summary: Elimina todos los temas.
 *     description: Elimina todos los temas.
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           type: integer
 *           description: cantidad de temas borrados.
 *           example: 12
*/
api.delete("/temas",
  [ md_auth.ensureAuth ,partialResponse()],
  temasController.destroyAll
);

module.exports = api;

