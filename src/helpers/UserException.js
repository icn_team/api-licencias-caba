class UserException extends Error {
    constructor(code, message) {
      super();
      this.code = code;
      this.message = message;
      this.name = "UserException";
    }
  }
  
  module.exports = UserException;