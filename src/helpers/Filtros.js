let filtros = (req) => {

    var limit = 30;
    var offset = 0;
    var order = 'id';
    var sort = 'asc';

    if(req.query.limit)  
      limit = parseInt(req.query.limit);
    if(req.query.offset) 
      offset = parseInt(req.query.offset);
    if(req.query.order) 
      order = req.query.order;
    if(req.query.sort) 
      sort = req.query.sort;

    if(req.query.limit == -1)  
      limit = 999999999;    

    return {limit,offset,order,sort};

}

module.exports = filtros;