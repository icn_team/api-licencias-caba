class CustomException extends Error {
    constructor(status, developerMessage, userMessage, errorCode='', moreInfo='') {
      super();
      this.status = status;
      this.developerMessage = developerMessage;
      this.userMessage = userMessage;
      this.errorCode = errorCode;
      this.moreInfo = moreInfo;
      this.name = "CustomException";
    }
  }

  module.exports = CustomException;